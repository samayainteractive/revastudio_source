$(document).ready(function(){

	$(window).on('resize',function() {
	    
	    var widthWindow = $(window).width();
	    var heighthest = 0;
	    var heighthestConntact = 0;

		if(widthWindow > 767){

			$.each($(".journal_list"), function(i, e){

				heighthest = $(this).height() > heighthest ? $(this).height() : heighthest; 

			});

			$.each($(".box_top_contact"), function(i, e){

				heighthestConntact = $(this).height() > heighthestConntact ? $(this).height() : heighthestConntact; 

			});

			// navigation
			$("#navigation").show();
			$(".sub-nav").css('display', '');

		}else{

			heighthest = "auto";
			heighthestConntact = "auto"; 

			// navigation
			$("#navigation").hide();

			// bar dropdown
			$("#bar").click(function(){
				$("#navigation").slideToggle();
			});

			// dropdowntoggle
			$(".dropdown-toggle").click(function(e){
				e.preventDefault();

				if(false == $(this).siblings().is(':visible')){
					$(".sub-nav").slideUp();
				}

				$(this).siblings().slideToggle();

			});
			

		}

		$(".journal_list").css('height', heighthest);
		$(".box_top_contact").css('height', heighthestConntact);

	}).trigger('resize');
	

});