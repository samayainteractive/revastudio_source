-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 07, 2017 at 03:33 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `revastudio`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id_about` int(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `title_seo` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image_about` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id_about`, `title`, `title_seo`, `description`, `image_about`) VALUES
(14, 'ABOUT US', 'about-us', 'Lorem Ipsum is simply dummy text of the printing and typesetting insdustry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. \r\n\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', 'clasic.jpg'),
(15, 'ABOUT US', 'about-us', 'Lorem Ipsum is simply dummy text of the printing and typesetting insdustry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. \r\n\r\nIt has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', 'sunrise.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `about_banner`
--

CREATE TABLE `about_banner` (
  `id_about_banner` int(5) NOT NULL,
  `title_about_banner` text NOT NULL,
  `image_about_banner` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_banner`
--

INSERT INTO `about_banner` (`id_about_banner`, `title_about_banner`, `image_about_banner`, `description`) VALUES
(1, 'MSSM ASSOCIATES', 'styles-room.jpg', '<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting insdustry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</strong><br />\r\n<br />\r\n<strong>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with dekstop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</strong></p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id_banner` int(5) NOT NULL,
  `title_banner` varchar(100) NOT NULL,
  `image_banner` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id_banner`, `title_banner`, `image_banner`) VALUES
(24, 'banner 2', 'm_house.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id_blogs` int(5) NOT NULL,
  `id_blogs_category` int(5) NOT NULL,
  `title_blogs` varchar(100) NOT NULL,
  `title_blogs_seo` varchar(100) NOT NULL,
  `image_blogs` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(100) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id_blogs`, `id_blogs_category`, `title_blogs`, `title_blogs_seo`, `image_blogs`, `description`, `meta_title`, `meta_keywords`, `meta_description`, `created_date`, `created_by`) VALUES
(4, 1, 'Example Title', 'example-title', 'imgCredit.png', '<p><span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pretium eros quis massa feugiat tincidunt. Donec quis tincidunt nulla, et dapibus tortor. Praesent nec hendrerit quam, vel posuere dui. Sed diam felis, feugiat ac odio et, dignissim vehicula dui. Nulla in gravida neque. Nunc viverra aliquam erat, nec consectetur tellus ultrices a. Duis sagittis sem nec faucibus auctor.&nbsp;</span><br />\r\n<br />\r\n<span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:medium">Pellentesque pretium dapibus ante, at sagittis ligula. Pellentesque dictum felis eget ultricies bibendum. Morbi arcu dolor, maximus id velit sed, elementum semper massa. Nullam ut consequat lectus. Sed id posuere massa. Sed dictum felis at lacus blandit, non dictum nisl egestas. Nunc mollis commodo ante vel scelerisque. Quisque gravida magna tortor, ut euismod turpis porta nec. Aliquam erat volutpat. Nullam laoreet hendrerit massa, non lobortis velit porttitor non. Fusce ornare quis ex eget facilisis. Phasellus ac est pharetra, semper ex at, tristique felis. Nunc dui mi, accumsan luctus ligula non, posuere elementum nibh. Aenean varius orci eget lorem scelerisque, consectetur bibendum libero commodo. Aliquam erat volutpat. Quisque lacus tortor, luctus vitae varius et, accumsan ac mi.</span></p>\r\n', 'Example Title', 'Example Title', 'Example Title', '2016-12-22 07:32:17', 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `blogs_category`
--

CREATE TABLE `blogs_category` (
  `id_blogs_category` int(5) NOT NULL,
  `title_blogs_category` varchar(100) NOT NULL,
  `title_blogs_category_seo` varchar(100) NOT NULL,
  `publish` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blogs_category`
--

INSERT INTO `blogs_category` (`id_blogs_category`, `title_blogs_category`, `title_blogs_category_seo`, `publish`) VALUES
(1, 'Blogs One', 'blogs-one', 1),
(2, 'Blogs Two', 'blogs-two', 1);

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id_careers` int(5) NOT NULL,
  `position` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`id_careers`, `position`, `description`) VALUES
(1, 'Grapich Designer', '<p>Requirements:</p>\r\n\r\n<ul>\r\n	<li>Available in microsoft word</li>\r\n	<li>Available in photoshop</li>\r\n	<li>Available in microsoft word &amp; Available in microsoft word</li>\r\n</ul>\r\n\r\n<h5>Task:</h5>\r\n\r\n<ul>\r\n	<li>Make graphich design more efficient</li>\r\n	<li>Mockup for exist image when be required</li>\r\n	<li>Available in microsoft word &amp; Available in microsoft word</li>\r\n</ul>\r\n'),
(2, 'Web Developer', '<p>Requirements:</p>\r\n\r\n<ul>\r\n	<li>Available in microsoft word</li>\r\n	<li>Available in photoshop</li>\r\n	<li>Available in microsoft word &amp; Available in microsoft word</li>\r\n</ul>\r\n\r\n<h5>Task:</h5>\r\n\r\n<ul>\r\n	<li>Make graphich design more efficient</li>\r\n	<li>Mockup for exist image when be required</li>\r\n	<li>Available in microsoft word &amp; Available in microsoft word</li>\r\n</ul>\r\n'),
(3, 'Sistem analyst', '<p>Requirements:</p>\r\n\r\n<ul>\r\n	<li>Available in microsoft word</li>\r\n	<li>Available in photoshop</li>\r\n	<li>Available in microsoft word &amp; Available in microsoft word</li>\r\n</ul>\r\n\r\n<h5>Task:</h5>\r\n\r\n<ul>\r\n	<li>Make graphich design more efficient</li>\r\n	<li>Mockup for exist image when be required</li>\r\n	<li>Available in microsoft word &amp; Available in microsoft word</li>\r\n</ul>\r\n'),
(4, 'Project Manager', '<p>Requirements:</p>\r\n\r\n<ul>\r\n	<li>Available in microsoft word</li>\r\n	<li>Available in photoshop</li>\r\n	<li>Available in microsoft word &amp; Available in microsoft word</li>\r\n</ul>\r\n\r\n<h5>Task:</h5>\r\n\r\n<ul>\r\n	<li>Make graphich design more efficient</li>\r\n	<li>Mockup for exist image when be required</li>\r\n	<li>Available in microsoft word &amp; Available in microsoft word</li>\r\n</ul>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id_contact` int(5) NOT NULL,
  `nation` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `telp` varchar(100) NOT NULL,
  `fax` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `map` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id_contact`, `nation`, `address`, `telp`, `fax`, `email`, `map`) VALUES
(1, 'England', 'London', '+447549521431', '+447549521431', 'rsi-london@revastudio.com', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1269730.2444163845!2d-1.1398377670187354!3d51.56929891320221!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a00baf21de75%3A0x52963a5addd52a99!2sLondon%2C+Inggris+Raya!5e0!3m2!1sid!2sid!4v1482661222519"  frameborder="0" style="border:0" allowfullscreen></iframe>'),
(2, 'Rusia', 'Moscow', '+74992570571', '+74992570571', 'rsi-moscow@revastudio.com', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d574867.141974347!2d37.0720854680302!3d55.74851697623669!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54afc73d4b0c9%3A0x3d44d6cc5757cf4c!2sMoskow%2C+Rusia!5e0!3m2!1sid!2sid!4v1482662133603" frameborder="0" style="border:0" allowfullscreen></iframe>'),
(3, 'Indonesia', 'Suren 1 no 10, Jakarta Selatan', '+62 81 745 7777', '+62 81 745 7777', 'rsi@revastudio.com', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.227047478944!2d106.81170421431338!3d-6.233772995487533!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f15ec449448f%3A0x8cdc70e974feeb42!2sJl.+Suren+1+No.10%2C+Rw.+Bar.%2C+Kby.+Baru%2C+Kota+Jakarta+Selatan%2C+Daerah+Khusus+Ibukota+Jakarta+12180!5e0!3m2!1sid!2sid!4v1482662099922" frameborder="0" style="border:0" allowfullscreen></iframe>'),
(4, 'Indonesia', 'Taman Hasanudin no 9, Semarang', '+62 81 7277 572', '+62 81 7277 572', 'rsi@revastudio.com', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.415278923153!2d110.40997061431807!3d-6.960240894971556!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e70f4bc860a9bd1%3A0x72eb78ba9a827662!2sJl.+Taman+Hasanudin%2C+Semarang+Utara%2C+Kota+Semarang%2C+Jawa+Tengah+50176!5e0!3m2!1sid!2sid!4v1482662202370" frameborder="0" style="border:0" allowfullscreen></iframe>'),
(5, 'Indonesia', 'Jalan Teuku Umar Barat (Marlboro) no. 1000<br>Kerobokan Kab. Bandung, Bali', '+74992570571', '+74992570571', 'rsi@revastudio.com', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3944.103742265687!2d115.18843831433091!3d-8.68168399376227!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd240cd66faaff7%3A0xc9ff56b6e7c3ca66!2sPT.+Duta+Intika+-+Teuku+Umar!5e0!3m2!1sid!2sid!4v1482662264272" frameborder="0" style="border:0" allowfullscreen></iframe>');

-- --------------------------------------------------------

--
-- Table structure for table `designers`
--

CREATE TABLE `designers` (
  `id_designers` int(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image_designers` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designers`
--

INSERT INTO `designers` (`id_designers`, `title`, `description`, `image_designers`) VALUES
(2, 'REVANO SATRIA', '<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting insdustry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.&nbsp;</strong><br />\r\n<br />\r\n<strong>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</strong></p>\r\n', 'white-house.jpg'),
(3, 'AURELIA', '<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting insdustry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.&nbsp;</strong><br />\r\n<br />\r\n<strong>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</strong></p>\r\n', 'Copenhagen.jpg'),
(4, 'STEPHANIE', '<p><strong>Lorem Ipsum is simply dummy text of the printing and typesetting insdustry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.&nbsp;</strong><br />\r\n<br />\r\n<strong>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</strong></p>\r\n', 'vancouver.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `journal`
--

CREATE TABLE `journal` (
  `id_journal` int(5) NOT NULL,
  `id_projects_category` int(5) NOT NULL,
  `title_projects` varchar(100) NOT NULL,
  `title_projects_seo` varchar(100) NOT NULL,
  `image_projects` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `team` text NOT NULL,
  `photographer` varchar(100) NOT NULL,
  `all_image` text NOT NULL,
  `journal` int(2) NOT NULL,
  `awards` text NOT NULL,
  `meta_title` varchar(100) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `order_no` int(11) NOT NULL,
  `date_projects` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `journal`
--

INSERT INTO `journal` (`id_journal`, `id_projects_category`, `title_projects`, `title_projects_seo`, `image_projects`, `description`, `status`, `location`, `team`, `photographer`, `all_image`, `journal`, `awards`, `meta_title`, `meta_keywords`, `meta_description`, `order_no`, `date_projects`) VALUES
(7, 1, 'BAZAAR WEDDING ORGANIZATION', 'bazaar-wedding-organization', 'relaxed-room.jpg', '', '', '', '', '', '<p><span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pretium eros quis massa feugiat tincidunt. Donec quis tincidunt nulla, et dapibus tortor. Praesent nec hendrerit quam, vel posuere dui. Sed diam felis, feugiat ac odio et, dignissim vehicula dui. Nulla in gravida neque. Nunc viverra aliquam erat, nec consectetur tellus ultrices a. Duis sagittis sem nec faucibus auctor.&nbsp;</span><br />\r\n<br />\r\n<span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:medium">Pellentesque pretium dapibus ante, at sagittis ligula. Pellentesque dictum felis eget ultricies bibendum. Morbi arcu dolor, maximus id velit sed, elementum semper massa. Nullam ut consequat lectus. Sed id posuere massa. Sed dictum felis at lacus blandit, non dictum nisl egestas. Nunc mollis commodo ante vel scelerisque. Quisque gravida magna tortor, ut euismod turpis porta nec. Aliquam erat volutpat. Nullam laoreet hendrerit massa, non lobortis velit porttitor non. Fusce ornare quis ex eget facilisis. Phasellus ac est pharetra, semper ex at, tristique felis. Nunc dui mi, accumsan luctus ligula non, posuere elementum nibh. Aenean varius orci eget lorem scelerisque, consectetur bibendum libero commodo. Aliquam erat volutpat. Quisque lacus tortor, luctus vitae varius et, accumsan ac mi.</span></p>\r\n', 0, '', 'assdas', 'asdasd', 'asdasd', 0, '2016-02-04'),
(8, 2, 'MURI WORLD RECORDS', 'muri-world-records', 'dinning-room.jpg', '', '', '', '', '', '<p><span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pretium eros quis massa feugiat tincidunt. Donec quis tincidunt nulla, et dapibus tortor. Praesent nec hendrerit quam, vel posuere dui. Sed diam felis, feugiat ac odio et, dignissim vehicula dui. Nulla in gravida neque. Nunc viverra aliquam erat, nec consectetur tellus ultrices a. Duis sagittis sem nec faucibus auctor.&nbsp;</span><br />\r\n<br />\r\n<span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:medium">Pellentesque pretium dapibus ante, at sagittis ligula. Pellentesque dictum felis eget ultricies bibendum. Morbi arcu dolor, maximus id velit sed, elementum semper massa. Nullam ut consequat lectus. Sed id posuere massa. Sed dictum felis at lacus blandit, non dictum nisl egestas. Nunc mollis commodo ante vel scelerisque. Quisque gravida magna tortor, ut euismod turpis porta nec. Aliquam erat volutpat. Nullam laoreet hendrerit massa, non lobortis velit porttitor non. Fusce ornare quis ex eget facilisis. Phasellus ac est pharetra, semper ex at, tristique felis. Nunc dui mi, accumsan luctus ligula non, posuere elementum nibh. Aenean varius orci eget lorem scelerisque, consectetur bibendum libero commodo. Aliquam erat volutpat. Quisque lacus tortor, luctus vitae varius et, accumsan ac mi.</span></p>\r\n', 0, '', 'asdas', 'asd', 'a', 1, '2016-12-21'),
(9, 2, 'BAZAAR WEDDING ORGANIZATION', 'bazaar-wedding-organization', 'dinning-room-guest.jpg', '', '', '', '', '', '<p><span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pretium eros quis massa feugiat tincidunt. Donec quis tincidunt nulla, et dapibus tortor. Praesent nec hendrerit quam, vel posuere dui. Sed diam felis, feugiat ac odio et, dignissim vehicula dui. Nulla in gravida neque. Nunc viverra aliquam erat, nec consectetur tellus ultrices a. Duis sagittis sem nec faucibus auctor.&nbsp;</span><br />\r\n<br />\r\n<span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:medium">Pellentesque pretium dapibus ante, at sagittis ligula. Pellentesque dictum felis eget ultricies bibendum. Morbi arcu dolor, maximus id velit sed, elementum semper massa. Nullam ut consequat lectus. Sed id posuere massa. Sed dictum felis at lacus blandit, non dictum nisl egestas. Nunc mollis commodo ante vel scelerisque. Quisque gravida magna tortor, ut euismod turpis porta nec. Aliquam erat volutpat. Nullam laoreet hendrerit massa, non lobortis velit porttitor non. Fusce ornare quis ex eget facilisis. Phasellus ac est pharetra, semper ex at, tristique felis. Nunc dui mi, accumsan luctus ligula non, posuere elementum nibh. Aenean varius orci eget lorem scelerisque, consectetur bibendum libero commodo. Aliquam erat volutpat. Quisque lacus tortor, luctus vitae varius et, accumsan ac mi.</span></p>\r\n', 0, '', 'gjghj', 'jhhgjhg', 'jhjh', 2, '2016-12-31'),
(10, 1, 'MURI WORLD RECORDS', 'muri-world-records', 'elle-decoration1.jpg', '', '', '', '', '', '<p><span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pretium eros quis massa feugiat tincidunt. Donec quis tincidunt nulla, et dapibus tortor. Praesent nec hendrerit quam, vel posuere dui. Sed diam felis, feugiat ac odio et, dignissim vehicula dui. Nulla in gravida neque. Nunc viverra aliquam erat, nec consectetur tellus ultrices a. Duis sagittis sem nec faucibus auctor.&nbsp;</span><br />\r\n<br />\r\n<span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:medium">Pellentesque pretium dapibus ante, at sagittis ligula. Pellentesque dictum felis eget ultricies bibendum. Morbi arcu dolor, maximus id velit sed, elementum semper massa. Nullam ut consequat lectus. Sed id posuere massa. Sed dictum felis at lacus blandit, non dictum nisl egestas. Nunc mollis commodo ante vel scelerisque. Quisque gravida magna tortor, ut euismod turpis porta nec. Aliquam erat volutpat. Nullam laoreet hendrerit massa, non lobortis velit porttitor non. Fusce ornare quis ex eget facilisis. Phasellus ac est pharetra, semper ex at, tristique felis. Nunc dui mi, accumsan luctus ligula non, posuere elementum nibh. Aenean varius orci eget lorem scelerisque, consectetur bibendum libero commodo. Aliquam erat volutpat. Quisque lacus tortor, luctus vitae varius et, accumsan ac mi.</span></p>\r\n', 0, '', 'test', 'test', 'test', 3, '2016-12-07'),
(11, 2, 'BAZAAR WEDDING ORGANIZATION', 'bazaar-wedding-organization', 'interior.jpg', '', '', '', '', '', '<p><img alt="" src="http://localhost/mssm_revisi/assets/upload/images/Copenhagen.jpg" style="float:left; height:550px; width:550px" /><img alt="" src="http://localhost/mssm_revisi/assets/upload/images/clasic-window.jpg" style="float:right; height:667px; width:500px" /></p>\r\n', 0, '', 'seo2', 'seo2', 'seo2', 4, '2016-12-05'),
(12, 1, '	MURI WORLD RECORDS', '	muri-world-records', 'relaxed-room1.jpg', '', '', '', '', '', '<p>&nbsp;</p>\r\n\r\n<p><img alt="" src="http://localhost/mssm_revisi/assets/upload/images/bathroom.jpg" style="float:left; height:667px; width:500px" /><img alt="" src="http://localhost/mssm_revisi/assets/upload/images/clasic-window.jpg" style="float:right; height:707px; width:530px" /></p>\r\n', 0, '', 'seo3', 'seo3', 'seo3', 5, '2016-12-15');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id_news` int(5) NOT NULL,
  `id_news_category` int(5) NOT NULL,
  `title_news` varchar(100) NOT NULL,
  `title_news_seo` varchar(100) NOT NULL,
  `image_news` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(100) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id_news`, `id_news_category`, `title_news`, `title_news_seo`, `image_news`, `description`, `meta_title`, `meta_keywords`, `meta_description`, `created_date`, `created_by`) VALUES
(3, 1, 'NEWS 2', 'news-2', 'building-view-corner.jpg', '<p><span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:14px">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pretium eros quis massa feugiat tincidunt. Donec quis tincidunt nulla, et dapibus tortor. Praesent nec hendrerit quam, vel posuere dui. Sed diam felis, feugiat ac odio et, dignissim vehicula dui. Nulla in gravida neque. Nunc viverra aliquam erat, nec consectetur tellus ultrices a. Duis sagittis sem nec faucibus auctor.&nbsp;</span><br />\r\n<br />\r\n<span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:14px">Pellentesque pretium dapibus ante, at sagittis ligula. Pellentesque dictum felis eget ultricies bibendum. Morbi arcu dolor, maximus id velit sed, elementum semper massa. Nullam ut consequat lectus. Sed id posuere massa. Sed dictum felis at lacus blandit, non dictum nisl egestas. Nunc mollis commodo ante vel scelerisque. Quisque gravida magna tortor, ut euismod turpis porta nec. Aliquam erat volutpat. Nullam laoreet hendrerit massa, non lobortis velit porttitor non. Fusce ornare quis ex eget facilisis. Phasellus ac est pharetra, semper ex at, tristique felis. Nunc dui mi, accumsan luctus ligula non, posuere elementum nibh. Aenean varius orci eget lorem scelerisque, consectetur bibendum libero commodo. Aliquam erat volutpat. Quisque lacus tortor, luctus vitae varius et, accumsan ac mi.</span></p>\r\n', 'NEWS 2', 'NEWS 2', 'NEWS 2', '2016-12-22 06:51:36', ''),
(4, 1, 'NEWS 1', 'news-1', 'building-view-corner1.jpg', '<p><span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:14px">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pretium eros quis massa feugiat tincidunt. Donec quis tincidunt nulla, et dapibus tortor. Praesent nec hendrerit quam, vel posuere dui. Sed diam felis, feugiat ac odio et, dignissim vehicula dui. Nulla in gravida neque. Nunc viverra aliquam erat, nec consectetur tellus ultrices a. Duis sagittis sem nec faucibus auctor.&nbsp;</span><br />\r\n<br />\r\n<span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:14px">Pellentesque pretium dapibus ante, at sagittis ligula. Pellentesque dictum felis eget ultricies bibendum. Morbi arcu dolor, maximus id velit sed, elementum semper massa. Nullam ut consequat lectus. Sed id posuere massa. Sed dictum felis at lacus blandit, non dictum nisl egestas. Nunc mollis commodo ante vel scelerisque. Quisque gravida magna tortor, ut euismod turpis porta nec. Aliquam erat volutpat. Nullam laoreet hendrerit massa, non lobortis velit porttitor non. Fusce ornare quis ex eget facilisis. Phasellus ac est pharetra, semper ex at, tristique felis. Nunc dui mi, accumsan luctus ligula non, posuere elementum nibh. Aenean varius orci eget lorem scelerisque, consectetur bibendum libero commodo. Aliquam erat volutpat. Quisque lacus tortor, luctus vitae varius et, accumsan ac mi.</span></p>\r\n', 'NEWS 1', 'NEWS 1', 'NEWS 1', '2016-12-22 07:18:42', 'Administrator'),
(5, 2, 'Publication 1', 'publication-1', 'building-view-corner2.jpg', '<p><span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:14px">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pretium eros quis massa feugiat tincidunt. Donec quis tincidunt nulla, et dapibus tortor. Praesent nec hendrerit quam, vel posuere dui. Sed diam felis, feugiat ac odio et, dignissim vehicula dui. Nulla in gravida neque. Nunc viverra aliquam erat, nec consectetur tellus ultrices a. Duis sagittis sem nec faucibus auctor.&nbsp;</span><br />\r\n<br />\r\n<span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:14px">Pellentesque pretium dapibus ante, at sagittis ligula. Pellentesque dictum felis eget ultricies bibendum. Morbi arcu dolor, maximus id velit sed, elementum semper massa. Nullam ut consequat lectus. Sed id posuere massa. Sed dictum felis at lacus blandit, non dictum nisl egestas. Nunc mollis commodo ante vel scelerisque. Quisque gravida magna tortor, ut euismod turpis porta nec. Aliquam erat volutpat. Nullam laoreet hendrerit massa, non lobortis velit porttitor non. Fusce ornare quis ex eget facilisis. Phasellus ac est pharetra, semper ex at, tristique felis. Nunc dui mi, accumsan luctus ligula non, posuere elementum nibh. Aenean varius orci eget lorem scelerisque, consectetur bibendum libero commodo. Aliquam erat volutpat. Quisque lacus tortor, luctus vitae varius et, accumsan ac mi.</span></p>\r\n', 'Publication 1', 'Publication 1', 'Publication 1', '2016-12-29 13:24:05', 'Administrator'),
(6, 2, 'Publication 2', 'publication-2', 'building-view-corner3.jpg', '<p><span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:14px">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pretium eros quis massa feugiat tincidunt. Donec quis tincidunt nulla, et dapibus tortor. Praesent nec hendrerit quam, vel posuere dui. Sed diam felis, feugiat ac odio et, dignissim vehicula dui. Nulla in gravida neque. Nunc viverra aliquam erat, nec consectetur tellus ultrices a. Duis sagittis sem nec faucibus auctor.&nbsp;</span><br />\r\n<br />\r\n<span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:14px">Pellentesque pretium dapibus ante, at sagittis ligula. Pellentesque dictum felis eget ultricies bibendum. Morbi arcu dolor, maximus id velit sed, elementum semper massa. Nullam ut consequat lectus. Sed id posuere massa. Sed dictum felis at lacus blandit, non dictum nisl egestas. Nunc mollis commodo ante vel scelerisque. Quisque gravida magna tortor, ut euismod turpis porta nec. Aliquam erat volutpat. Nullam laoreet hendrerit massa, non lobortis velit porttitor non. Fusce ornare quis ex eget facilisis. Phasellus ac est pharetra, semper ex at, tristique felis. Nunc dui mi, accumsan luctus ligula non, posuere elementum nibh. Aenean varius orci eget lorem scelerisque, consectetur bibendum libero commodo. Aliquam erat volutpat. Quisque lacus tortor, luctus vitae varius et, accumsan ac mi.</span></p>\r\n', 'Publication 2', 'Publication 2', 'Publication 2', '2016-12-29 13:25:44', 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `news_category`
--

CREATE TABLE `news_category` (
  `id_news_category` int(5) NOT NULL,
  `title_news_category` varchar(100) NOT NULL,
  `title_news_category_seo` varchar(100) NOT NULL,
  `publish` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_category`
--

INSERT INTO `news_category` (`id_news_category`, `title_news_category`, `title_news_category_seo`, `publish`) VALUES
(1, 'News', 'news', 1),
(2, 'Publication', 'publication', 1);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id_projects` int(5) NOT NULL,
  `id_projects_category` int(5) NOT NULL,
  `title_projects` varchar(100) NOT NULL,
  `title_projects_seo` varchar(100) NOT NULL,
  `image_projects` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `team` text NOT NULL,
  `photographer` varchar(100) NOT NULL,
  `all_image` text NOT NULL,
  `journal` int(2) NOT NULL,
  `awards` text NOT NULL,
  `meta_title` varchar(100) NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `order_no` int(11) NOT NULL,
  `date_projects` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id_projects`, `id_projects_category`, `title_projects`, `title_projects_seo`, `image_projects`, `description`, `status`, `location`, `team`, `photographer`, `all_image`, `journal`, `awards`, `meta_title`, `meta_keywords`, `meta_description`, `order_no`, `date_projects`) VALUES
(6, 1, 'PROJECT TITLE 1', 'project-title-1', 'view-stairs.jpg', '', '', '', '', '', '<p><img alt="" src="http://localhost/mssm_revisi/assets/upload/images/mini-flower.jpg" style="float:left; height:515px; width:515px" /><img alt="" src="http://localhost/mssm_revisi/assets/upload/images/mini-flower.jpg" style="float:right; height:515px; width:515px" /></p>\r\n', 1, '', 'PROJECT TITLE 1', 'PROJECT TITLE 1', 'PROJECT TITLE 1', 0, '2016-12-01'),
(8, 2, 'PROJECT TITLE 2', 'project-title-2', 'building-view-corner.jpg', '', '', '', '', '', '<p><img alt="" src="http://localhost/mssm_revisi/assets/upload/images/mini-flower.jpg" style="float:left; height:520px; width:520px" /><img alt="" src="http://localhost/mssm_revisi/assets/upload/images/mini-flower.jpg" style="float:right; height:520px; width:520px" /></p>\r\n', 1, '', 'PROJECT TITLE 2', 'PROJECT TITLE 2', 'PROJECT TITLE 2', 1, '2016-12-08'),
(9, 3, 'PROJECT TITLE 3', 'project-title-3', 'view-stairs1.jpg', '', '', '', '', '', '<p><span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pretium eros quis massa feugiat tincidunt. Donec quis tincidunt nulla, et dapibus tortor. Praesent nec hendrerit quam, vel posuere dui. Sed diam felis, feugiat ac odio et, dignissim vehicula dui. Nulla in gravida neque. Nunc viverra aliquam erat, nec consectetur tellus ultrices a. Duis sagittis sem nec faucibus auctor.&nbsp;</span><br />\r\n<br />\r\n<span style="color:rgb(79, 79, 79); font-family:adobe_garamond_pro_regular,serif; font-size:medium">Pellentesque pretium dapibus ante, at sagittis ligula. Pellentesque dictum felis eget ultricies bibendum. Morbi arcu dolor, maximus id velit sed, elementum semper massa. Nullam ut consequat lectus. Sed id posuere massa. Sed dictum felis at lacus blandit, non dictum nisl egestas. Nunc mollis commodo ante vel scelerisque. Quisque gravida magna tortor, ut euismod turpis porta nec. Aliquam erat volutpat. Nullam laoreet hendrerit massa, non lobortis velit porttitor non. Fusce ornare quis ex eget facilisis. Phasellus ac est pharetra, semper ex at, tristique felis. Nunc dui mi, accumsan luctus ligula non, posuere elementum nibh. Aenean varius orci eget lorem scelerisque, consectetur bibendum libero commodo. Aliquam erat volutpat. Quisque lacus tortor, luctus vitae varius et, accumsan ac mi.</span></p>\r\n', 1, '', 'PROJECT TITLE 3', 'PROJECT TITLE 3', 'PROJECT TITLE 3', 2, '2016-12-01');

-- --------------------------------------------------------

--
-- Table structure for table `projects_category`
--

CREATE TABLE `projects_category` (
  `id_projects_category` int(5) NOT NULL,
  `title_projects_category` varchar(100) NOT NULL,
  `title_projects_category_seo` varchar(100) NOT NULL,
  `publish` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects_category`
--

INSERT INTO `projects_category` (`id_projects_category`, `title_projects_category`, `title_projects_category_seo`, `publish`) VALUES
(1, 'Residential', 'residential', 1),
(2, 'Commercial', 'commercial', 1),
(3, 'Office', 'office', 1);

-- --------------------------------------------------------

--
-- Table structure for table `seo`
--

CREATE TABLE `seo` (
  `id_seo` int(5) NOT NULL,
  `menu` varchar(100) NOT NULL,
  `title` text NOT NULL,
  `keyword` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seo`
--

INSERT INTO `seo` (`id_seo`, `menu`, `title`, `keyword`, `description`) VALUES
(1, 'Home', '', '', ''),
(2, 'Journal', '', '', ''),
(3, 'Designer', '', '', ''),
(4, 'News', '', '', ''),
(5, 'Works', '', '', ''),
(6, 'Contact', '', '', ''),
(7, 'About', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id_services` int(5) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE `social_media` (
  `id_social_media` int(5) NOT NULL,
  `title_social_media` varchar(100) NOT NULL,
  `link_social_media` varchar(100) NOT NULL,
  `image_social_media` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

CREATE TABLE `template` (
  `id_template` int(2) NOT NULL,
  `title` varchar(100) NOT NULL,
  `img_template` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `template`
--

INSERT INTO `template` (`id_template`, `title`, `img_template`) VALUES
(1, 'Logo', 'MSSMassociates.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_users` int(5) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `level` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_users`, `username`, `password`, `name`, `email`, `level`, `status`) VALUES
(4, 'superadmin', '12345', 'Administrator', 'admin@gmail.com', 1, 1),
(6, 'andriana', '12345', 'Andriana', 'contact.andriana@gmail.com', 2, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id_about`);

--
-- Indexes for table `about_banner`
--
ALTER TABLE `about_banner`
  ADD PRIMARY KEY (`id_about_banner`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id_banner`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id_blogs`);

--
-- Indexes for table `blogs_category`
--
ALTER TABLE `blogs_category`
  ADD PRIMARY KEY (`id_blogs_category`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id_careers`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indexes for table `designers`
--
ALTER TABLE `designers`
  ADD PRIMARY KEY (`id_designers`);

--
-- Indexes for table `journal`
--
ALTER TABLE `journal`
  ADD PRIMARY KEY (`id_journal`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id_news`);

--
-- Indexes for table `news_category`
--
ALTER TABLE `news_category`
  ADD PRIMARY KEY (`id_news_category`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id_projects`);

--
-- Indexes for table `projects_category`
--
ALTER TABLE `projects_category`
  ADD PRIMARY KEY (`id_projects_category`);

--
-- Indexes for table `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`id_seo`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id_services`);

--
-- Indexes for table `social_media`
--
ALTER TABLE `social_media`
  ADD PRIMARY KEY (`id_social_media`);

--
-- Indexes for table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id_template`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id_about` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `about_banner`
--
ALTER TABLE `about_banner`
  MODIFY `id_about_banner` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id_banner` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id_blogs` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `blogs_category`
--
ALTER TABLE `blogs_category`
  MODIFY `id_blogs_category` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id_careers` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id_contact` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `designers`
--
ALTER TABLE `designers`
  MODIFY `id_designers` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `journal`
--
ALTER TABLE `journal`
  MODIFY `id_journal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id_news` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `news_category`
--
ALTER TABLE `news_category`
  MODIFY `id_news_category` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id_projects` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `projects_category`
--
ALTER TABLE `projects_category`
  MODIFY `id_projects_category` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `seo`
--
ALTER TABLE `seo`
  MODIFY `id_seo` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id_services` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `social_media`
--
ALTER TABLE `social_media`
  MODIFY `id_social_media` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `template`
--
ALTER TABLE `template`
  MODIFY `id_template` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_users` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
