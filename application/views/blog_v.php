<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Revastudio</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/master.css">
</head>
<body>

	<?php $this->load->view('header_v'); ?>
	
	<article class="main-box">

		<!-- content journal -->
		<?php
		foreach($blog->result() as $data){ ?>

			<section class="content-journal">
				<h3><?php echo $data->title_blogs ?></h3>
				<p><?php echo $data->created_date ?></p>
				<section class="journal10">
					<a href="<?php echo base_url(); ?>blog/details/<?php echo $data->id_blogs ?>/<?php echo url_title($data->title_blogs) ?>"><img src="<?php echo base_url(); ?>all_picture/blogs/medium/<?php echo $data->image_blogs ?>" alt="<?php echo $data->image_blogs ?>"></a>
				</section>
				<div class="read_more">
					<a href="<?php echo base_url(); ?>blog/details/<?php echo $data->id_blogs ?>/<?php echo url_title($data->title_blogs) ?>">Read More</a>
				</div>
			</section>	
		
		<?php
		}
		?>
		
		<div class="text_center">
			<?php echo $pagination ?>	
		</div>

	</article>

	<?php $this->load->view('footer_v.php'); ?>
	
</body>
</html>