<!-- head -->
<header>
	<div class="box-head">
		<div class="box-social-media">
			<a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
			<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
		</div>
		<a href="<?php echo base_url(); ?>" class="img-head">
			<img src="<?php echo base_url(); ?>assets/img/img-head.png" alt="img-head">
		</a>
		<form action="#" class="form-search">
			<div>
				<input type="text" name="search">
				<button type="submit" name="submit-search"><i class="fa fa-search"></i></button>
			</div>
		</form>
	</div>
</header>

<!-- main navbar -->
<nav class="main-nav">
	<div class="box-bar">
		<i class="fa fa-bars" aria-hidden="true" id="bar"></i>
	</div>
	<ul id="navigation">
		<li><a href="<?php echo base_url(); ?>">HOME</a></li>
		<li><a href="<?php echo base_url(); ?>journal">JOURNAL</a></li>
		<li><a href="<?php echo base_url(); ?>designer">DESIGNER</a></li>
		<li>
			<a href="#" class="dropdown-toggle"><i class="fa fa-caret-down" aria-hidden="true"></i>NEWS</a>
			<ul class="sub-nav">
				<?php
				foreach($newsCategory->result() as $data){ ?>
					<li><a href="<?php echo base_url(); ?>news/index/<?php echo $data->id_news_category ?>/<?php echo $data->title_news_category ?>"><?php echo $data->title_news_category ?></a></li> <?php
				}
				?>
			</ul>
		</li>
		<li><a href="<?php echo base_url(); ?>works">WORKS</a></li>
		<li>
			<a href="#" class="dropdown-toggle"><i class="fa fa-caret-down" aria-hidden="true"></i>CONTACT</a>
			<ul class="sub-nav">
				<li><a href="<?php echo base_url(); ?>contact">Contact</a></li>
				<li><a href="<?php echo base_url(); ?>career">Career</a></li>
			</ul>
		</li>
		<li><a href="<?php echo base_url(); ?>about">ABOUT</a></li>
	</ul>
</nav>