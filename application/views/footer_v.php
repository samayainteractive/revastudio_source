<footer>
		<ul>
		<li><a href="<?php echo base_url(); ?>">HOME</a></li>
		<li><a href="<?php echo base_url(); ?>blog">BLOG</a></li>
		<li><a href="<?php echo base_url(); ?>career">CAREER</a></li>
		<li><a href="<?php echo base_url(); ?>contact">CONTACT</a></li>
		<li><a href="<?php echo base_url(); ?>about">ABOUT</a></li>
		<li>
			<form action="#">
				<input type="text" name="email" placeholder="ENTER EMAIL">
			</form>
		</li>
	</ul>
</footer>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="https://use.fontawesome.com/7915c342a1.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/master.js"></script>

<script>
	$(document).ready(function(){

		// grid filter
		$('#gridFilter').mixItUp();

	});
</script>