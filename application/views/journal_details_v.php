<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Revastudio</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/master.css">
</head>
<body>

	<?php $this->load->view('header_v'); ?>
	
	<article class="main-box">

		<!-- content journal -->
		<?php
		foreach($journalSelected->result() as $data){ ?>
			<section class="content-journal">
				<h3><?php echo $data->title_projects ?></h3>
				<p><?php echo $data->date_projects ?></p>
				<section class="journal10">
					<img class="main_image_content" src="<?php echo base_url(); ?>all_picture/journal/original/<?php echo $data->image_projects ?>" alt="<?php echo $data->image_projects ?>">
				</section>
				<div class="description_content">
					<?php echo $data->all_image ?>
				</div>
				<!-- <section class="second-journal">
					<section class="second-left"><a href="#"><img src="<?php echo base_url(); ?>assets/img/bathroom.jpg" alt="bathroom"></a></section>
					<section class="second-right"><a href="#"><img src="<?php echo base_url(); ?>assets/img/clasic-window.jpg" alt="clasic-window"></a></section>
				</section>
				<section class="treeth-journal">
					<section class="treeth-left"><a href="#"><img src="<?php echo base_url(); ?>assets/img/cup.jpg" alt="cup"></a></section>
					<section class="treeth-right"><a href="#"><img src="<?php echo base_url(); ?>assets/img/tree.jpg" alt="tree"></a></section>
				</section>
				<section class="second-journal">
					<section class="second-left"><a href="#"><img src="<?php echo base_url(); ?>assets/img/bathroom.jpg" alt="bathroom"></a></section>
					<section class="second-right"><a href="#"><img src="<?php echo base_url(); ?>assets/img/clasic-window.jpg" alt="clasic-window"></a></section>
				</section> -->
			</section>
		<?php
		}
		?>

	</article>

	<?php $this->load->view('footer_v.php'); ?>
	
</body>
</html>