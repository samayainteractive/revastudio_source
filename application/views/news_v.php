<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Revastudio</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/master.css">
</head>
<body>

	<?php $this->load->view('header_v'); ?>
	
	<article class="main-box">
		<?php
		foreach($news->result() as $data){ ?>

			<section class="main-news">
				<section class="content-news">
					<h3><?php echo $data->title_news ?></h3>
					<p><?php echo $data->created_date ?></p>
					<section><a href="<?php echo base_url(); ?>news/details/<?php echo $data->id_news ?>/<?php echo url_title($data->title_news) ?>"><img src="<?php echo base_url(); ?>all_picture/news/medium/<?php echo $data->image_news ?>" alt="building-view-corner"></a></section>
					<div class="read_more">
						<a href="<?php echo base_url(); ?>news/details/<?php echo $data->id_news ?>/<?php echo url_title($data->title_news) ?>">Read More</a>
					</div>
				</section>
			</section> <?php

		}
		?>
	</article>
	
	<?php $this->load->view('footer_v.php'); ?>
	
</body>
</html>