<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
   
</head>

<body>

    <div id="wrapper">
	
        <?php $this->load->view('backend/header_v'); ?>

        <div id="page-wrapper">
            <div class="container-fluid">
			
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Social Media <small>Update</small>
                        </h1>
                        <ol class="breadcrumb">
                          
                            <li class="active">
                                <i class="fa fa-fw fa-file"></i> Social Media
                            </li>
                        </ol>
                    </div>
                </div>
				
				<?php foreach($social_media_update as $data_0): ?>
				<?php echo form_open_multipart('page/social_media_update_process/'.$data_0->id_social_media, 'onsubmit="return ValidationSocialMediaUpdate()"'); ?>
							
				<div class="row">
					
					<div class="col-lg-8">
				
						<div class="form-group">
							<p>Title</p>
							<input type="text" id="title_social_media" name="title_social_media" value="<?php echo $data_0->title_social_media ?>" class="form-control">
						</div>
						<div class="form-group">
							<p>Link</p>
							<input type="text" id="link_social_media" name="link_social_media" value="<?php echo $data_0->link_social_media ?>" class="form-control">
						</div>
						<div class="form-group">
							<p>Image</p>
							<div class="alert alert-info" style="padding:8px;">
							  <strong>Info!</strong> Max size 2 MB and format file jpg or png
							</div>
							<p><img src="<?php echo base_url(); ?>all_picture/social_media/<?php echo $data_0->image_social_media ?>" style="width:220px;"></p> 
							
							<input type="file" name="gambar">
						</div>
						
					</div>
					
				</div>
				<input type="submit" value="Save" class="btn btn-success" style="width:100px;">
				</form>	
				<?php endforeach ?>
            </div>
        </div>
    </div>
	
	<?php $this->load->view('backend/footer_v'); ?>
	 
</body>
</html>

<?php
	}
?>

