<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
</head>

<body>

    <div id="wrapper">
	
        <?php $this->load->view('backend/header_v'); ?>

        <div id="page-wrapper">
            <div class="container-fluid">
			
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            MY ACCOUNT <small>All File</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-users"></i> Users
                            </li>
                        </ol>
                    </div>
                </div>
				
				<div class="row">
					<?php foreach($myaccount as $data): ?>
					
					
					<div class="col-lg-4">
						<div class="form-group">
							<p>Username</p>
							<input type="text" name="username" value="<?php echo $data->username ?>" class="form-control" required disabled>
						</div>
						<div class="form-group">
							<p>Full Name</p>
							
							<input type="text" name="name" value="<?php echo $data->name ?>" class="form-control" required disabled>
						</div>
						<div class="form-group">
							<p>Email</p>
							<input type="email" name="email" value="<?php echo $data->email ?>" class="form-control" required disabled>
						</div>
						
					</div>
					<div class="col-lg-4">
						
						<div class="form-group">
							Level :
							<?php
								if($data->level == '1'){
									?>
										Super Admin	
									<?php 
								}else{
									?>
										Admin
									<?php 
								}
							?>
							
						</div>
						<div class="form-group">
							Activation :
							<?php
								if($data->status =='1'){
									?>
										Yes
									<?php 
								}else{
									?>
										No
									<?php 
								}
							?>
						</div>
					</div>
				</div>
				<a href="<?php echo base_url(); ?>page/myaccount_update/<?php echo $data->id_users ?>">
					<input type="submit" value="Edit" class="btn btn-success" style="width:100px;">
				</a>
				<?php endforeach ?>	
            </div>
        </div>
		
    </div>

	<?php $this->load->view('backend/footer_v'); ?>
	
</body>
</html>
<?php
	}
?>
