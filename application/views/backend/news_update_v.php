<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>

   <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
</head>

<body>

    <div id="wrapper">
	
        <?php $this->load->view('backend/header_v'); ?>

        <div id="page-wrapper">
            <div class="container-fluid">
			
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            News <small>Update</small>
                        </h1>
                        <ol class="breadcrumb">
                          
                            <li class="active">
                                <i class="fa fa-fw fa-file"></i> News
                            </li>
                        </ol>
                    </div>
                </div>
				
				<?php foreach($news_update as $data_0): ?>
				<?php echo form_open_multipart('page/news_update_process/'.$data_0->id_news, 'onsubmit="return ValidationNewsUpdate()"'); ?>
							
				<div class="row">
					
					<div class="col-lg-8">
						
						<div class="form-group">
						<p>News Category</p>
						<select id="news_category" name="news_category" style="margin-top:10px;" class="form-control">
						<?php foreach($news_category as $data): ?>
							<?php
								if($data->id_news_category==0){
							?>
								<option value="0" selected>- Select -</option>
							<?php 
								}
							?>	
							<?php
								if($data->id_news_category==$data_0->id_news_category){
							?>
								<option value=<?php echo $data->id_news_category ?> selected><?php echo $data->title_news_category ?></option>
							<?php 
								}else{
							?>
								
								<option value=<?php echo $data->id_news_category ?>><?php echo $data->title_news_category ?></option>
									
							<?php 
								}
							?>
						<?php endforeach ?>
						</select>
						</div>
						
						
						<div class="form-group">
							<p>Title</p>
							<input type="text" id="title_news" name="title_news" value="<?php echo $data_0->title_news ?>" class="form-control">
						</div>
						<div class="form-group">
							<p>Image</p>
							<div class="alert alert-info" style="padding:8px;">
							  <strong>Info!</strong> Max size 2 MB and format file jpg or png
							</div>
							<p><img src="<?php echo base_url(); ?>all_picture/news/small/<?php echo $data_0->image_news ?>"></p> 
							
							<input type="file" name="gambar">
						</div>
						
						<div class="form-group">
							<p>Description</p>
							<textarea name="description" class="form-control" id="editor"><?php echo $data_0->description ?></textarea>
						</div>
						
						<div class="form-group">
							<p>Meta Title</p>
							<input type="text" id="meta_title" name="meta_title" class="form-control" value="<?php echo $data_0->meta_title ?>">
						</div>
						<div class="form-group">
							<p>Meta Keywords</p>
							<input type="text" id="meta_keywords" name="meta_keywords" class="form-control" value="<?php echo $data_0->meta_keywords ?>">
						</div>
						<div class="form-group">
							<p>Meta Description</p>
							<textarea name="meta_description" id="meta_description" class="form-control"><?php echo $data_0->meta_description ?></textarea>
						</div>
						
					</div>
					
				</div>
				<input type="submit" value="Save" class="btn btn-success" style="width:100px;">
				</form>	
				<?php endforeach ?>
            </div>
        </div>
    </div>
	
	<?php $this->load->view('backend/footer_v'); ?>
	 
</body>
</html>

<?php
	}
?>

