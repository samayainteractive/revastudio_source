<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>
	
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>

</head>

<body>

    <div id="wrapper">

        <?php 
			$this->load->view('backend/header_v');
	    ?>

        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Sorry ! <small></small>
                        </h1>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                           Max file 2 MB and format file JPG or PNG !
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
	
	<?php $this->load->view('backend/footer_v'); ?>
	
</body>
</html>

<?php
	}
?>
