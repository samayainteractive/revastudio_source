<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>CMS Panel</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
<!-- CSS Bootstrap -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/backend/css/master.css" rel="stylesheet">

</head>
<body style="background:#F5F5F5">
	<div class="container" id="containerLogin">
		<div id="titleLogin">WARNING !</div>
		<div id="descLogin">
			<div id="wrapperLogin" style="color:black; padding-bottom:20px;">
					Sorry can not login.<br />
					Please check Your username and password<br />
			</div>
		</div>
	</div>
</body>
</html>
