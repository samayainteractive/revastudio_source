<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
</head>

<body>

    <div id="wrapper">
	
        <?php $this->load->view('backend/header_v'); ?>

        <div id="page-wrapper">
            <div class="container-fluid">
			
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            SEO PAGE <small>All File</small>
                        </h1>
                        <ol class="breadcrumb">
                          
                            <li class="active">
                                <i class="fa fa-fw fa-cogs"></i> SEO
                            </li>
                        </ol>
                    </div>
                </div>
				
				
				
				<div class="row">
					<div class="col-lg-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped" style="font-size:13px;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Menu</th>
                                        <th>Title</th>
										<th>Keyword</th>
										<th>Description</th>
										<th style="width:200px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
									<?php $no=1 ?>
									<?php foreach($seo as $data): ?>
										<tr>
											<td><?php echo $no++; ?></td>
											<td><?php echo $data->menu ?></td>
											<td><?php echo $data->title ?></td>
											<td><?php echo $data->keyword ?> </td>
											<td><?php echo $data->description ?> </td>
									
											<td>
												<a href="<?php echo base_url(); ?>page/seo_update/<?php echo $data->id_seo ?>">
													<button type="button" class="btn btn-sm btn-primary">&nbsp;&nbsp;&nbsp;<i class="fa fa-edit"></i> Edit&nbsp;&nbsp;&nbsp;</button>
												</a>
												
											</td>
										</tr>
										
									<?php endforeach ?>
                              
                                </tbody>
                            </table>
                        </div>
                    </div>
				</div>
				
            </div>
        </div>
		
    </div>
	
	<?php $this->load->view('backend/footer_v'); ?>

</body>
</html>
<?php
	}
?>
