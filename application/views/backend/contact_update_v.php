<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
</head>

<body>

    <div id="wrapper">
	
        <?php $this->load->view('backend/header_v'); ?>

        <div id="page-wrapper">
            <div class="container-fluid">
			
                <!-- Page Heading -->
               <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Contact <small>Update</small>
                        </h1>
                        <ol class="breadcrumb">
                          
                            <li class="active">
                                <i class="fa fa-fw fa-cogs"></i> Contact
                            </li>
                        </ol>
                    </div>
                </div>
				
				<div class="row">
					<?php foreach($contact_update as $data): ?>
					<?php echo form_open('page/contact_update_process/'.$data->id_contact, 'onsubmit="return ValidationContact()"'); ?>
					<div class="col-lg-6">
						<form method="post" action="contact_update_process">
						<div class="form-group">
							<p>Nation</p>
							<input type="text" id="nation" name="nation" value="<?php echo $data->nation ?>" class="form-control">
						</div>
						<div class="form-group">
							<p>Address</p>
							<textarea id="address" name="address"  class="form-control"><?php echo $data->address ?></textarea>
						</div>
						<div class="form-group">
							<p>Telp</p>
							<input type="text" id="telp" name="telp" class="form-control" value="<?php echo $data->telp ?>">
						</div>
						<div class="form-group">
							<p>Fax</p>
							<input type="text" id="fax" name="fax" class="form-control" value="<?php echo $data->fax ?>">
						</div>
						<div class="form-group">
							<p>Email</p>
							<input type="text" id="email" name="email" class="form-control" value="<?php echo $data->email ?>">
						</div>
						<div class="form-group">
							<p>Map</p>
							<input type="text" id="map" name="map" class="form-control" value="<?php echo $data->map ?>">
						</div>
							
						<input type="submit" value="Save" class="btn btn-success" style="width:100px;">
						</form>		
					</div>
				</div>
				
				<?php endforeach ?>	
            </div>
        </div>
		
    </div>

	<?php $this->load->view('backend/footer_v'); ?>
	
</body>
</html>
<?php
	}
?>
