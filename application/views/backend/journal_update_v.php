<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>

   <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
</head>

<body>

    <div id="wrapper">
	
        <?php $this->load->view('backend/header_v'); ?>

        <div id="page-wrapper">
            <div class="container-fluid">
			
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Journal <small>Update</small>
                        </h1>
                        <ol class="breadcrumb">
                          
                            <li class="active">
                                <i class="fa fa-fw fa-file"></i> Journal
                            </li>
                        </ol>
                    </div>
                </div>
				
				<?php foreach($journal_update as $data_0): ?>
				<?php echo form_open_multipart('page/journal_update_process/'.$data_0->id_journal, 'onsubmit="return ValidationProjectsUpdate()"'); ?>
							
				<div class="row">
					
					<div class="col-lg-8">
				
						<div class="form-group">
						<p>Projects Category <span style="color:red; float:right;">(*) Must be Filled</span></p>
						<select id="projects_category" name="projects_category" style="margin-top:10px;" class="form-control">
						<?php foreach($projects_category as $data): ?>
							<?php
								if($data->id_projects_category==0){
							?>
								<option value="0" selected>- Select -</option>
							<?php 
								}
							?>	
							<?php
								if($data->id_projects_category==$data_0->id_projects_category){
							?>
								<option value=<?php echo $data->id_projects_category ?> selected><?php echo $data->title_projects_category ?></option>
							<?php 
								}else{
							?>
								
								<option value=<?php echo $data->id_projects_category ?>><?php echo $data->title_projects_category ?></option>
									
							<?php 
								}
							?>
						<?php endforeach ?>
						</select>
						</div>
						
						<div class="form-group">
							<p>Title <span style="color:red; float:right;">(*) Must be Filled</span></p>
							<input type="text" id="title_projects" name="title_projects" value="<?php echo $data_0->title_projects ?>" class="form-control">
						</div>

						<div class="form-group">
							<p>Date </p>
							<input type="text" name="date_projects" value="<?php echo $data_0->date_projects ?>" placeholder="0000-00-00" id="dp1" class="form-control">
						</div>
						
						<div class="form-group">
							<p>Image </p>
							<div class="alert alert-info" style="padding:8px;">
							  <strong>Info!</strong> Max size 2 MB and format file jpg or png
							</div>
							<p><img src="<?php echo base_url(); ?>all_picture/journal/medium/<?php echo $data_0->image_projects ?>"></p> 
							
							<input type="file" name="gambar">
						</div>
						
						<!--
						<div class="form-group">
							<p>Description</p>
							<textarea name="description" class="form-control" id="editor"><?php echo $data_0->description ?></textarea>
						</div>
						
						<div class="form-group">
							<p>Status</p>
							<input type="text" name="status" value="<?php echo $data_0->status ?>" class="form-control">
						</div>
						
						<div class="form-group">
							<p>Location</p>
							<input type="text" name="location" value="<?php echo $data_0->location ?>" class="form-control">
						</div>
						<div class="form-group">
							<p>Team</p>
							<input type="text" name="team" value="<?php echo $data_0->team ?>" class="form-control">
						</div>
						<div class="form-group">
							<p>Photographer</p>
							<input type="text" name="photographer" value="<?php echo $data_0->photographer ?>" class="form-control">
						</div>
						-->
						<div class="form-group">
							<p>All Image</p>
							<textarea name="all_image" class="form-control" id="editor1"><?php echo $data_0->all_image ?></textarea>
						</div>
						<div class="form-group">
							<p>Publish Journal</p>
							<?php
								if($data_0 ->journal == 0){
									?>
										<div class="radio">
											<label>
												<input type="radio" name="journal"  value="0" checked>Yes
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" name="journal" value="1">No
											</label>
										</div>
									<?php 
								}else{
									?>
									<div class="radio">
										<label>
											<input type="radio" name="journal"  value="0">Yes
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="journal" value="1" checked>No
										</label>
									</div>
									<?php 
								}
							?>
							
						</div>
						
						<!--
						<div class="form-group">
							<p>Awards</p>
							<textarea id="editor2" name="awards" class="form-control"><?php echo $data_0->awards ?></textarea>
						</div>
						-->
						
						<div class="form-group">
							<p>Meta Title <span style="color:red; float:right;">(*) Must be Filled</span></p>
							<input type="text" id="meta_title" name="meta_title" class="form-control" value="<?php echo $data_0->meta_title ?>">
						</div>
						<div class="form-group">
							<p>Meta Keywords <span style="color:red; float:right;">(*) Must be Filled</span></p>
							<input type="text" id="meta_keywords" name="meta_keywords" class="form-control" value="<?php echo $data_0->meta_keywords ?>">
						</div>
						<div class="form-group">
							<p>Meta Description <span style="color:red; float:right;">(*) Must be Filled</span></p>
							<textarea id="meta_description" name="meta_description" class="form-control"><?php echo $data_0->meta_description ?></textarea>
						</div>
						
					</div>
					
				</div>
				<input type="submit" value="Save" class="btn btn-success" style="width:100px;">
				</form>	
				<?php endforeach ?>
            </div>
        </div>
    </div>
	
	<?php $this->load->view('backend/footer_v'); ?>
	 
</body>
</html>

<?php
	}
?>

