<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
</head>

<body>

    <div id="wrapper">
	
        <?php $this->load->view('backend/header_v'); ?>

        <div id="page-wrapper">
            <div class="container-fluid">
			
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            SEO PAGE <small>Create</small>
                        </h1>
                        <ol class="breadcrumb">
                          
                            <li class="active">
                                <i class="fa fa-fw fa-cogs"></i> SEO
                            </li>
                        </ol>
                    </div>
                </div>
				
				<form method="post" action="create_seo_process" onsubmit="return ValidationSeo()">
				<div class="row">
					<div class="col-lg-8">
						<div class="form-group">
							<p>Menu</p>
							<input id="menu" type="text" name="menu" class="form-control">
						</div>
						<div class="form-group">
							<p>Title</p>
							<input id="title" type="text" name="title" class="form-control">
						</div>
						<div class="form-group">
							<p>Keywords</p>
							<input id="keyword" type="text" name="keyword" class="form-control">
						</div>
						<div class="form-group">
							<p>Description</p>
							<textarea id="description" name="description" class="form-control"></textarea>
						</div>
					</div>
				</div>
				<input type="submit" value="Save" class="btn btn-success" style="width:100px;">
				</form>	
        </div>
    </div>
	
	
	<?php $this->load->view('backend/footer_v'); ?>
	 
</body>
</html>

<?php
	}
?>

