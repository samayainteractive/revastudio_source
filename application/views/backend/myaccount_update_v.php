<?php 
$username = $this->session->userdata('username');
$password = $this->session->userdata('password');
if (empty($username) AND empty($password)){
	echo"Please login !";
}else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS Panel</title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/iconSmall5.png"/>
</head>

<body>

    <div id="wrapper">
	
        <?php $this->load->view('backend/header_v'); ?>

        <div id="page-wrapper">
            <div class="container-fluid">
			
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            MY ACCOUNT <small>All File</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-users"></i> Users
                            </li>
                        </ol>
                    </div>
                </div>
				
				<div class="row">
					<?php foreach($myaccount_update as $data): ?>
					<?php echo form_open('page/myaccount_update_process/'.$data->id_users, 'onsubmit="return ValidationUsers()"'); ?>
					
					<div class="col-lg-4">
						<div class="form-group">
							<p>Username</p>
							<input type="text" id="username" name="username" value="<?php echo $data->username ?>" class="form-control">
						</div>
						<div class="form-group">
							<p>Password</p>
							<input type="text" id="password" name="password" value="<?php echo $data->password ?>"  class="form-control"> 
						</div>
						<div class="form-group">
							<p>Full Name</p>
							<input type="text" id="name" name="name" value="<?php echo $data->name ?>" class="form-control">
						</div>
						<div class="form-group">
							<p>Email</p>
							<input type="email" id="email" name="email" value="<?php echo $data->email ?>" class="form-control">
						</div>
						
					</div>
					<div class="col-lg-4">
						
						<div class="form-group">
							<p>Level</p>
							<?php
								if($data->level == '1'){
									?>
										<div class="radio">
											<label>
												<input type="radio" name="level" id="optionsRadios1" value="1" checked>Super Admin
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" name="level" id="optionsRadios1" value="2">Admin
											</label>
										</div>
									<?php 
								}else{
									?>
										<div class="radio">
											<label>
												<input type="radio" name="level" id="optionsRadios1" value="1">Super Admin
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" name="level" id="optionsRadios1" value="2" checked>Admin
											</label>
										</div>
									<?php 
								}
							?>
							
						</div>
						<div class="form-group">
							<p>Activation</p>
							<?php
								if($data->status =='1'){
									?>
										<div class="radio">
											<label>
												<input type="radio" name="status" id="optionsRadios1" value="1" checked>Yes
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" name="status" id="optionsRadios1" value="2">No
											</label>
										</div>
									<?php 
								}else{
									?>
									<div class="radio">
										<label>
											<input type="radio" name="status" id="optionsRadios1" value="1">Yes
										</label>
									</div>
									<div class="radio">
										<label>
											<input type="radio" name="status" id="optionsRadios1" value="2" checked>No
										</label>
									</div>
									<?php 
								}
							?>
						</div>
					</div>
				</div>
				
					<input type="submit" value="Save" class="btn btn-success" style="width:100px;">
				</form>
				<?php endforeach ?>	
            </div>
        </div>
		
    </div>

	<?php $this->load->view('backend/footer_v'); ?>
	
</body>
</html>
<?php
	}
?>
