<!DOCTYPE html>

<html>

<head>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE-edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Revastudio</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/master.css">

	<style>



		#gridFilter .mix{

			display: none;

		}



		.category-nav .active{

			font-style: italic;

		}



	</style>

</head>

<body>



	<?php $this->load->view('header_v'); ?>

	

	<article class="main-box">

		<section class="main-work">

			
			<!-- category nav -->

			<nav class="category-nav">

				<ul>

					<li class="filter active" data-filter="all"><p>ALL</p></li>

					<?php

					foreach($workCategory->result() as $data){ ?>



						<li class="filter" data-filter=".cat_<?php echo $data->id_projects_category ?>"><p><?php echo $data->title_projects_category ?></p></li> <?php



					}

					?>

				</ul>

			</nav>

			<!-- slide category -->

			<section class="slide-category">

				<ul id="gridFilter">

					<?php

					foreach($works->result() as $data){ ?>



						<li class="mix cat_<?php echo $data->id_projects_category ?>"><a href="<?php echo base_url(); ?>works/details/<?php echo $data->id_projects ?>/<?php echo url_title($data->title_projects) ?>"><img src="<?php echo base_url(); ?>all_picture/projects/medium/<?php echo $data->image_projects ?>" alt="<?php echo $data->image_projects ?>"></a></li>	<?php

					

					}

					?>

					<div class="gap"></div>

				</ul>

			</section>


			<!-- <?php

			foreach($works->result() as $data){ ?>



				<section class="project-content">

					<h3><?php echo $data->title_projects ?></h3>

					<p><?php echo $data->date_projects ?></p>

					<section class="journal10">

						<a href="<?php echo base_url(); ?>works/details/<?php echo $data->id_projects ?>/<?php echo url_title($data->title_projects) ?>">

							<img src="<?php echo base_url(); ?>all_picture/projects/medium/<?php echo $data->image_projects ?>" alt="<?php echo $data->image_projects ?>">

						</a>

					</section>

					<div class="read_more">

						<a href="<?php echo base_url(); ?>works/details/<?php echo $data->id_projects ?>/<?php echo url_title($data->title_projects) ?>">Read More</a>

					</div>

				</section> <?php



			}

			?> -->



		</section>

	</article>

	

	<?php $this->load->view('footer_v.php'); ?>

	

</body>

</html>