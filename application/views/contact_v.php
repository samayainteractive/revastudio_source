<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Revastudio</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/master.css">
</head>
<body>

	<?php $this->load->view('header_v'); ?>
	
	<article class="main-box">
		<section class="main-work">
			<h3 class="title_page">CONTACT</h3>
			<div class="row_contact">
				
				<?php
				foreach($contact->result() as $data){ ?>

					<div class="grid_contact">
						<div class="box_contact">
							<div class="box_top_contact">
								<h4><?php echo $data->address ?></h4>
								<a href="mailto:<?php echo $data->email ?>"><?php echo $data->email ?></a>
								<a href="tel://<?php echo $data->telp ?>"><?php echo $data->telp ?></a>
							</div>
							<div class="embed-responsive embed-responsive-16by9">
							  	<?php echo $data->map ?>
							</div>
						</div>
					</div> <?php

				}
				?>

			</div>
		</section>
	</article>
	
	<?php $this->load->view('footer_v.php'); ?>
	
</body>
</html>