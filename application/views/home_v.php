<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Revastudio</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/master.css">

	<style>

		#gridFilter .mix{
			display: none;
		}

		.category-nav .active{
			font-style: italic;
		}

	</style>
</head>
<body>

	<?php $this->load->view('header_v'); ?>
	
	<article>

		<!-- carousel -->
		<section class="box-carousel">
			<?php
			foreach($banner->result() as $data){ ?>

				<img src="<?php echo base_url(); ?>all_picture/banner/<?php echo $data->image_banner ?>" alt="<?php echo $data->image_banner ?>"> <?php

			}
			?>
		</section>

		<!-- category nav -->
		<nav class="category-nav">
			<ul>
				<li class="filter active" data-filter="all"><p>ALL</p></li>
				<?php
				foreach($workCategory->result() as $data){ ?>

					<li class="filter" data-filter=".cat_<?php echo $data->id_projects_category ?>"><p><?php echo $data->title_projects_category ?></p></li> <?php

				}
				?>
			</ul>
		</nav>

		<!-- slide category -->
		<section class="slide-category">
			<ul id="gridFilter">
				<?php
				foreach($work->result() as $data){ ?>

					<li class="mix cat_<?php echo $data->id_projects_category ?>"><a href="<?php echo base_url(); ?>works/details/<?php echo $data->id_projects ?>/<?php echo url_title($data->title_projects) ?>"><img src="<?php echo base_url(); ?>all_picture/projects/medium/<?php echo $data->image_projects ?>" alt="<?php echo $data->image_projects ?>"></a></li>	<?php
				
				}
				?>
				<div class="gap"></div>
			</ul>
		</section>

		<section class="list-content">
			<h3>JOURNAL & PUBLICATIONS</h3>
			<hr>
			<ul>
				<?php
				foreach($journal->result() as $data){ ?>
					
					<li class="journal_list">
						<a href="<?php echo base_url(); ?>journal/details/<?php echo $data->id_journal ?>/<?php echo url_title($data->title_projects) ?>">
							<figure>
								<img src="<?php echo base_url(); ?>all_picture/journal/medium/<?php echo $data->image_projects ?>" alt="<?php echo $data->image_projects ?>">
								<figcaption>
									<h3><?php echo $data->title_projects ?></h3>
									<p><?php echo $data->date_projects ?></p>
								</figcaption>
							</figure>
						</a>
					</li>

				<?php } ?>
			</ul>
		</section>
	</article>

	<aside>
		<article class="associates">
			<section>
				<?php
				foreach($about->result() as $data){ ?>

					<h3><?php echo $data->title_about_banner ?></h3>
					<hr>
					<p>
						<?php echo $data->description ?>
					</p> <?php

				}
				?>
			</section>
			<section class="associates-img">
				<a href="<?php echo base_url(); ?>">
					<?php
					foreach($logo->result() as $data){ ?>

						<img src="<?php echo base_url(); ?>all_picture/template/<?php echo $data->img_template ?>" alt="<?php echo $data->img_template ?>"> <?php

					}
					?>
				</a>
			</section>
		</article>
	</aside>

	<?php $this->load->view('footer_v.php'); ?>

	
	
</body>
</html>