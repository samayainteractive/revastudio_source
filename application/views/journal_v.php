<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Revastudio</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/master.css">
</head>
<body>

	<?php $this->load->view('header_v'); ?>
	
	<article class="main-box">

		<!-- content journal -->
		<?php
		foreach($journal->result() as $data){ ?>

			<section class="content-journal">
				<h3><?php echo $data->title_projects ?></h3>
				<p><?php echo $data->date_projects ?></p>
				<section class="journal10">
					<a href="<?php echo base_url(); ?>journal/details/<?php echo $data->id_journal ?>/<?php echo url_title($data->title_projects) ?>"><img src="<?php echo base_url(); ?>all_picture/journal/original/<?php echo $data->image_projects ?>" alt="white-bathroom"></a>
				</section>
				<div class="read_more">
					<a href="<?php echo base_url(); ?>journal/details/<?php echo $data->id_journal ?>/<?php echo url_title($data->title_projects) ?>">Read More</a>
				</div>
			</section>	
		
		<?php
		}
		?>
		
		<div class="text_center">
			<?php echo $pagination ?>	
		</div>

	</article>

	<?php $this->load->view('footer_v.php'); ?>
	
</body>
</html>