<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Career extends CI_Controller {

	// Construct
	public function __construct()
	{
		parent::__construct();
		$this->load->model('access_m');
		
	}

	public function index()
	{
		$data['career'] = $this->access_m->getCareer();
		$data['newsCategory'] = $this->access_m->getNewsCategory();

		$this->load->view('career_v', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */