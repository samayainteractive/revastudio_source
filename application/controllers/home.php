<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	// Construct
	public function __construct()
	{
		parent::__construct();
		$this->load->model('access_m');
		
	}

	public function index()
	{
		$data['journal'] = $this->access_m->journalLimit(6);
		$data['banner'] = $this->access_m->getMainBanner();
		$data['about'] = $this->access_m->about_banner();
		$data['logo'] = $this->access_m->getLogo();
		$data['workCategory'] = $this->access_m->getWorkCategory();
		$data['work'] = $this->access_m->getWorks();
		$data['newsCategory'] = $this->access_m->getNewsCategory();
		
		$this->load->view('home_v',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */