<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Journal extends CI_Controller {

	// Construct
	public function __construct()
	{
		parent::__construct();
		$this->load->model('access_m');

		$this->load->library('pagination');
		
	}
	
	public function index()
	{

		//pagination settings
        $config['base_url'] = site_url('journal/index');
        $config['total_rows'] = $this->access_m->journalAll()->num_rows();
        $config['per_page'] = "6";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        //call the model function to get the department data
        $data['journal'] = $this->access_m->journalOrderLimit($config["per_page"], $data['page']);           

        $data['pagination'] = $this->pagination->create_links();
        $data['newsCategory'] = $this->access_m->getNewsCategory();

        //load the department_view
		$this->load->view('journal_v', $data);
	}

	function details($id){
		
		$data['journalSelected'] = $this->access_m->journalSelected($id);
                $data['newsCategory'] = $this->access_m->getNewsCategory();

		$this->load->view('journal_details_v', $data);	
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */