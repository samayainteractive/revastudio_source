<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {

	// Construct
	public function __construct()
	{
		parent::__construct();
		$this->load->model('access_m');
		
	}

	public function index()
	{
		$data['about'] = $this->access_m->getAbout();
		$data['about_banner'] = $this->access_m->about_banner();
		$data['newsCategory'] = $this->access_m->getNewsCategory();

		$this->load->view('about_v', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */