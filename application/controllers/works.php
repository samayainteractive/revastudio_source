<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Works extends CI_Controller {



	// Construct

	public function __construct()

	{

		parent::__construct();

		$this->load->model('access_m');

		

	}



	public function index()

	{

		$data['workCategory'] = $this->access_m->getWorkCategory();
		
		$data['works'] = $this->access_m->getWorks();

		$data['newsCategory'] = $this->access_m->getNewsCategory();



		$this->load->view('works_v', $data);

	}



	function details($id){



		$data['worksSelected'] = $this->access_m->getWorksSelected($id);

		$data['newsCategory'] = $this->access_m->getNewsCategory();



		$this->load->view('works_details_v', $data);

	}

}



/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */