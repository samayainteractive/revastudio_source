<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {

	// Construct
	public function __construct()
	{
		parent::__construct();
		$this->load->model('page_m');
		
	}

	
	// Cek Login users  
	function cek_users()
	{
		$username = $this->input->post('username'); 
		$password = $this->input->post('password'); 
		$pass = $password; 
		$q = $this->db->query("select * from users where username='$username' AND password='$pass' AND status='1'");
		if ($q->num_rows() == 1 ) {	
			$id_users = $q->row()->id_users;
			$this->session->set_userdata('id_users',$id_users);
			$data['id_users'] = "$id_users";
			
			$username = $q->row()->username;
			$this->session->set_userdata('username',$username);
			$data['username'] = "$username";
			
			$level = $q->row()->level;
			$this->session->set_userdata('level',$level);
			$data['level'] = "$level";
			
			$name = $q->row()->name;
			$this->session->set_userdata('name',$name);
			$data['name'] = "$name";
						
			redirect('page/home');
			
		}
		else{
			$this->load->view('backend/login_validasi_v');
		}
	}
	
	// logout
	public function logout() {
		$this->session->sess_destroy();
		redirect('cms');
	}
		
	public function home()
	{
		$this->load->view('backend/home_v');
	}
	
	public function users()
	{
		$data['users']=$this->page_m->users();
		$this->load->view('backend/users_v', $data);
	}
	
	public function create_users()
	{
		$this->load->view('backend/users_create_v');
	}
	
	// Users Create Process
	
	function create_users_process()
	{
		$data=array(
			
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'level' => $this->input->post('level'),
			'status' => $this->input->post('status')
		);	
		$this->page_m->create_users($data);
		redirect('page/users');
	}

	// Users Update
	function users_update($id_users)
	{
		$data['users_update']=$this->page_m->users_update($id_users);
		$this->load->view('backend/users_update_v', $data);
	}
	
	// Users Update Process
	function users_update_process($id_users)
	{
		$data['users_update_process']=$this->page_m->users_update_process($id_users);
		redirect('page/users');
	}
	
	// Users Delete
	function users_delete()
	{
		$this->page_m->users_delete();
		redirect('page/users');
	}
	
	// My account
	function myaccount()
	{
		$data['myaccount']=$this->page_m->myaccount();
		$this->load->view('backend/myaccount_v',$data);
	}
	
	// My Account update
	function myaccount_update($id_users)
	{
		$data['myaccount_update']=$this->page_m->myaccount_update($id_users);
		$this->load->view('backend/myaccount_update_v', $data);
	}
	
	// My Account update process
	function myaccount_update_process($id_users)
	{
		$data['myaccount_update_process']=$this->page_m->myaccount_update_process($id_users);
		redirect('page/myaccount');
	}
		
	// seo
	function seo()
	{
		
    	$data['seo']=$this->page_m->seo();  
        $this->load->view('backend/seo_v',$data);
    
	}
	
	// seo
	function create_seo()
	{
		$this->load->view('backend/seo_create_v');
	}
	
	// Create seo process 
	function create_seo_process(){
		$data=array(
				'menu' => $this->input->post('menu'),
				'title' => $this->input->post('title'),
				'keyword' => $this->input->post('keyword'),
				'description' => $this->input->post('description')
			);
		$this->page_m->create_seo($data);
		redirect('page/seo');
			
	}
	
	// seo update
	function seo_update($id_seo)
	{
		$data['seo_update']=$this->page_m->seo_update($id_seo);
		$this->load->view('backend/seo_update_v', $data);
	}
	
	// seo Update Process
	function seo_update_process($id_seo)
	{
		$data['seo_update_process']=$this->page_m->seo_update_process($id_seo);
		redirect('page/seo');
	}
	
	// seo Delete
	function seo_delete($id_seo)
	{
		$this->page_m->seo_delete($id_seo);
		redirect('page/seo');
	}

	// banner
	function banner()
	{
		$data['banner']=$this->page_m->banner();
		$this->load->view('backend/banner_v',$data);
	}
	
	// Create banner
	function create_banner()
	{
		$this->load->view('backend/banner_create_v');
	}
	
	// Create banner
	function create_banner_process(){
		
		$this->load->library('image_lib');
		$url = './all_picture/';    //path image
		if($_FILES['gambar']['error']==3)  //if No file was uploaded.
		return false;  
			
		$config['upload_path'] = $url."banner/";        
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']        = 2048; 
		
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		$files = $this->upload->data();
		
		if( $this->upload->do_upload('gambar',$config) )
		{    
			$files = $this->upload->data();

			$data = array('upload_data' => $this->upload->data());
			$hasil = str_replace (" ",  "_", $files['file_name']);
			$data=array(	
				'title_banner' => $this->input->post('title_banner'),
				'image_banner' =>  $hasil
			);
			
			$this->page_m->create_banner($data);
			redirect('page/banner');
			
		}else{
			$this->load->view('backend/image_validasi_v');
		}
	}
	
	// banner update
	function banner_update($id_banner)
	{
		$data['banner_update']=$this->page_m->banner_update($id_banner);
		$this->load->view('backend/banner_update_v', $data);
	}
	
	// banner Update Process
	function banner_update_process($id_banner)
	{
		$data['banner_update_process']=$this->page_m->banner_update_process($id_banner);
		//redirect('page/banner');
	}

	// banner Delete
	function banner_delete($id_banner)
	{
		$this->page_m->banner_delete($id_banner);
		redirect('page/banner');
	}
	
	
	// social_media
	function social_media()
	{
		$data['social_media']=$this->page_m->social_media();
		$this->load->view('backend/social_media_v',$data);
	}
	
	// Create social_media
	function create_social_media()
	{
		$this->load->view('backend/social_media_create_v');
	}
	
	// Create social_media
	function create_social_media_process(){
		
		$this->load->library('image_lib');
		$url = './all_picture/';    //path image
		if($_FILES['gambar']['error']==3)  //if No file was uploaded.
		return false;  
			
		$config['upload_path'] = $url."social_media/";        
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']        = 2048; 
		
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		$files = $this->upload->data();
		
		if( $this->upload->do_upload('gambar',$config) )
		{    
			$files = $this->upload->data();

			$data = array('upload_data' => $this->upload->data());
			$hasil = str_replace (" ",  "_", $files['file_name']);
			$data=array(	
				'title_social_media' => $this->input->post('title_social_media'),
				'link_social_media' => $this->input->post('link_social_media'),
				'image_social_media' =>  $hasil
			);
			
			$this->page_m->create_social_media($data);
			redirect('page/social_media');
			
		}else{
			$this->load->view('backend/image_validasi_v');
		}
	}
	
	// social_media update
	function social_media_update($id_social_media)
	{
		$data['social_media_update']=$this->page_m->social_media_update($id_social_media);
		$this->load->view('backend/social_media_update_v', $data);
	}
	
	// social_media Update Process
	function social_media_update_process($id_social_media)
	{
		$data['social_media_update_process']=$this->page_m->social_media_update_process($id_social_media);
		redirect('page/social_media');
	}

	// social_media Delete
	function social_media_delete($id_social_media)
	{
		$this->page_m->social_media_delete($id_social_media);
		redirect('page/social_media');
	}
	
	
	// projects_category
	function projects_category()
	{
		$data['projects_category']=$this->page_m->projects_category();
		$this->load->view('backend/projects_category_v',$data);
	}
	
	// projects_category
	function create_projects_category()
	{
		$this->load->view('backend/projects_category_create_v');
	}
	
	// Create projects_category process 
	function create_projects_category_process(){
		$title_projects_category_seo = seo($this->input->post('title_projects_category'));
		$data=array(
				'title_projects_category' => $this->input->post('title_projects_category'),
				'title_projects_category_seo' => $title_projects_category_seo,
				'publish' => $this->input->post('publish')
			);
		$this->page_m->create_projects_category($data);
		redirect('page/projects_category');
			
	}
	
	// projects_category update
	function projects_category_update($id_projects_category)
	{
		$data['projects_category_update']=$this->page_m->projects_category_update($id_projects_category);
		$this->load->view('backend/projects_category_update_v', $data);
	}
	
	// projects_category Update Process
	function projects_category_update_process($id_projects_category)
	{
		$data['projects_category_update_process']=$this->page_m->projects_category_update_process($id_projects_category);
		redirect('page/projects_category');
	}
	
	// projects_category Delete
	function projects_category_delete()
	{
		$this->page_m->projects_category_delete();
		redirect('page/projects_category');
	}
	
	// projects
	function projects()
	{
		$data['projects']=$this->page_m->projects();
		$this->load->view('backend/projects_v',$data);
	}
	
	// Create projects
	function create_projects()
	{
		$data['projects_category']=$this->page_m->projects_category();
		$this->load->view('backend/projects_create_v', $data);
	}
	
	// Create projects
	function create_projects_process(){
		
/*
		$this->load->library('image_lib');
		$url = './all_picture/projects/';    //path image
		if($_FILES['gambar']['error']==3)  //if No file was uploaded.
		return false;  
			
		$config['upload_path'] = $url."original/";        
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']        = 2048; 
		$lokasi_file    = $_FILES['gambar']['tmp_name'];
		
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		$files = $this->upload->data();
		$fileNameResize = $config['upload_path'].$files['file_name'];
		
		if( $this->upload->do_upload('gambar',$config) )
		{    
			$files = $this->upload->data();
			
			$fileNameResize = $config['upload_path'].$files['file_name'];
			$size =  array(                
						array('name'    => 'small','width'    => 100, 'height'    => 100, 'quality'    => '100%'),
						array('name'    => 'medium','width'    => 250, 'height'    => 250, 'quality'    => '100%')
					);
			$resize = array();
			foreach($size as $r){                
				$resize = array(
					"width"            => $r['width'],
					"height"        => $r['height'],
					"quality"        => $r['quality'],
					"source_image"    => $fileNameResize,
					"new_image"        => $url.$r['name'].'/'.$files['file_name']
				);
				$this->image_lib->initialize($resize);
				if(!$this->image_lib->resize())                    
					die($this->image_lib->display_errors());
			}

			$data = array('upload_data' => $this->upload->data());
			$hasil = str_replace (" ",  "_", $files['file_name']);
			$title_projects_seo = seo($this->input->post('title_projects'));
			$data=array(	
				'id_projects_category' => $this->input->post('projects_category'),
				'title_projects' => $this->input->post('title_projects'),
				'title_projects_seo' => $title_projects_seo,
				'image_projects' =>  $hasil,
				'description' => $this->input->post('description'),
				'status' => $this->input->post('status'),
				'location' => $this->input->post('location'),
				'team' => $this->input->post('team'),
				'photographer' => $this->input->post('photographer'),
				'all_image' => $this->input->post('all_image'),
				'journal' => $this->input->post('journal'),
				'awards' => $this->input->post('awards'),
				'meta_title' => $this->input->post('meta_title'),
				'meta_keywords' => $this->input->post('meta_keywords'),
				'meta_description' => $this->input->post('meta_description')
			);
			
			$this->page_m->create_projects($data);
			redirect('page/projects');
			
		}else{
			$this->load->view('backend/image_validasi_v');
		}
*/

$this->load->library('image_lib');
		$url = './all_picture/projects/';   
		$urlDuplicate = './all_picture/journal/';   
		//path image
		if($_FILES['gambar']['error']==3)  //if No file was uploaded.
		return false;  
			
		/*
		$config['upload_path'] = $url."original/";        
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']        = 2048; 
		$lokasi_file    = $_FILES['gambar']['tmp_name'];
		
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		$files = $this->upload->data();
		$fileNameResize = $config['upload_path'].$files['file_name'];
		*/
		
		$config = array();
		$config['upload_path'] = $url."original/";        
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']        = 2048; 
		$this->load->library('upload', $config, 'projects'); // Create custom object for cover upload
		$this->projects->initialize($config);
		$projects = $this->projects->do_upload('gambar');
		
		$config = array();
		$config['upload_path'] = $urlDuplicate."original/";        
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']        = 2048; 
		$this->load->library('upload', $config, 'journal'); // Create custom object for cover upload
		$this->journal->initialize($config);
		$journal = $this->journal->do_upload('gambar');
		
		
		
	
		
		if( $projects && $journal)
		{    
			$filesprojects = $this->projects->data();
			$filesjournal = $this->journal->data();
			
			
			
			
			$fileNameResize = $config['upload_path'].$filesprojects['file_name'];
			$fileNameResizejournal = $config['upload_path'].$filesjournal['file_name'];
			
			$size =  array(                
						array('name'    => 'small','width'    => 100, 'height'    => 100, 'quality'    => '100%'),
						array('name'    => 'medium','width'    => 250, 'height'    => 250, 'quality'    => '100%')
					);
						
					
			$resize = array();
			$resizejournal = array();
			
	
			foreach($size as $r){                
				$resize = array(
					"width"            => $r['width'],
					"height"        => $r['height'],
					"quality"        => $r['quality'],
					"source_image"    => $fileNameResize,
					"new_image"        => $url.$r['name'].'/'.$filesprojects['file_name']
				);
				$this->image_lib->initialize($resize);
				if(!$this->image_lib->resize())                    
					die($this->image_lib->display_errors());
			}
			
			foreach($size as $a){                
				$resize = array(
					"width"            => $a['width'],
					"height"        => $a['height'],
					"quality"        => $a['quality'],
					"source_image"    => $fileNameResizejournal,
					"new_image"        => $urlDuplicate.$a['name'].'/'.$filesjournal['file_name']
				);
				$this->image_lib->initialize($resize);
				if(!$this->image_lib->resize())                    
					die($this->image_lib->display_errors());
			}
			
			

			$data = array('upload_data' => $this->projects->data());
			$data = array('upload_data' => $this->journal->data());
			
			$hasil = str_replace (" ",  "_", $filesprojects['file_name']);
			
			
			
				$title_projects_seo = seo($this->input->post('title_projects'));
				$data=array(	
					'id_projects_category' => $this->input->post('projects_category'),
					'title_projects' => $this->input->post('title_projects'),
					'title_projects_seo' => $title_projects_seo,
					'image_projects' =>  $hasil,
					/*
					'description' => $this->input->post('description'),
					'status' => $this->input->post('status'),
					'location' => $this->input->post('location'),
					'team' => $this->input->post('team'),
					'photographer' => $this->input->post('photographer'),
					*/
					'all_image' => $this->input->post('all_image'),
					'journal' => $this->input->post('journal'),
					/*
					'awards' => $this->input->post('awards'),
					*/
					'meta_title' => $this->input->post('meta_title'),
					'meta_keywords' => $this->input->post('meta_keywords'),
					'meta_description' => $this->input->post('meta_description'),
'order_no' => $this->input->post('order_no'),
'date_projects' => $this->input->post('date_projects')
				);
				
				$this->page_m->create_projects($data);
				$this->page_m->create_journal($data);
				redirect('page/projects');
			
			
		}else{
			$this->load->view('backend/image_validasi_v');
		}
	}
	
	// projects update
	function projects_update($id_projects)
	{
		$data['projects_update']=$this->page_m->projects_update($id_projects);
		$data['projects_category']=$this->page_m->projects_category();
		$this->load->view('backend/projects_update_v', $data);
	}
	
	// projects Update Process
	function projects_update_process($id_projects)
	{
		$data['projects_update_process']=$this->page_m->projects_update_process($id_projects);
		redirect('page/projects');
	}

	// projects Delete
	function projects_delete($id_projects)
	{
		$this->page_m->projects_delete($id_projects);
		redirect('page/projects');
	}
	
	// news_category
	function news_category()
	{
		$data['news_category']=$this->page_m->news_category();
		$this->load->view('backend/news_category_v',$data);
	}
	
	// news_category
	function create_news_category()
	{
		$this->load->view('backend/news_category_create_v');
	}
	
	// Create news_category process 
	function create_news_category_process(){
		$title_news_category_seo = seo($this->input->post('title_news_category'));
		$data=array(
				'title_news_category' => $this->input->post('title_news_category'),
				'title_news_category_seo' => $title_news_category_seo,
				'publish' => $this->input->post('publish')
			);
		$this->page_m->create_news_category($data);
		redirect('page/news_category');
			
	}
	
	// news_category update
	function news_category_update($id_news_category)
	{
		$data['news_category_update']=$this->page_m->news_category_update($id_news_category);
		$this->load->view('backend/news_category_update_v', $data);
	}
	
	// news_category Update Process
	function news_category_update_process($id_news_category)
	{
		$data['news_category_update_process']=$this->page_m->news_category_update_process($id_news_category);
		redirect('page/news_category');
	}
	
	// news_category Delete
	function news_category_delete()
	{
		$this->page_m->news_category_delete();
		redirect('page/news_category');
	}
	
	
	// news
	function news()
	{
		$data['news']=$this->page_m->news();
		$this->load->view('backend/news_v',$data);
	}
	
	// Create news
	function create_news()
	{
		$data['news_category']=$this->page_m->news_category();
		$this->load->view('backend/news_create_v', $data);
	}
	
	// Create news
	function create_news_process(){
		
		$this->load->library('image_lib');
		$url = './all_picture/news/';    //path image
		if($_FILES['gambar']['error']==3)  //if No file was uploaded.
		return false;  
			
		$config['upload_path'] = $url."original/";        
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']        = 2048; 
		$lokasi_file    = $_FILES['gambar']['tmp_name'];
		
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		$files = $this->upload->data();
		$fileNameResize = $config['upload_path'].$files['file_name'];
		
		if( $this->upload->do_upload('gambar',$config) )
		{    
			$files = $this->upload->data();
			$fileNameResize = $config['upload_path'].$files['file_name'];
				$size =  array(                
							array('name'    => 'small','width'    => 100, 'height'    => 100, 'quality'    => '100%'),
							array('name'    => 'medium','width'    => 500, 'height'    => 500, 'quality'    => '100%')
						);
				$resize = array();
				foreach($size as $r){                
					$resize = array(
						"width"            => $r['width'],
						"height"        => $r['height'],
						"quality"        => $r['quality'],
						"source_image"    => $fileNameResize,
						"new_image"        => $url.$r['name'].'/'.$files['file_name']
					);
					$this->image_lib->initialize($resize);
					if(!$this->image_lib->resize())                    
						die($this->image_lib->display_errors());
				}

			$data = array('upload_data' => $this->upload->data());
			$hasil = str_replace (" ",  "_", $files['file_name']);
			$title_news_seo = seo($this->input->post('title_news'));
			$data=array(	
				'id_news_category' => $this->input->post('news_category'),
				'title_news' => $this->input->post('title_news'),
				'title_news_seo' => $title_news_seo,
				'image_news' =>  $hasil,
				'description' => $this->input->post('description'),
				'meta_title' => $this->input->post('meta_title'),
				'meta_keywords' => $this->input->post('meta_keywords'),
				'meta_description' => $this->input->post('meta_description'),
				'created_by' => $this->session->userdata('name'),
			);
			
			$this->page_m->create_news($data);
			redirect('page/news');
			
		}else{
			$this->load->view('backend/image_validasi_v');
		}
	}
	
	// news update
	function news_update($id_news)
	{
		$data['news_update']=$this->page_m->news_update($id_news);
		$data['news_category']=$this->page_m->news_category();
		$this->load->view('backend/news_update_v', $data);
	}
	
	// news Update Process
	function news_update_process($id_news)
	{
		$data['news_update_process']=$this->page_m->news_update_process($id_news);
		redirect('page/news');
	}

	// news Delete
	function news_delete($id_news)
	{
		$this->page_m->news_delete($id_news);
		redirect('page/news');
	}
	
	
	// contact
	function contact()
	{
		$data['contact']=$this->page_m->contact();
		$this->load->view('backend/contact_v',$data);
	}
	
	// contact
	function create_contact()
	{
		$this->load->view('backend/contact_create_v');
	}
	
	// Create contact process 
	function create_contact_process(){
		$data=array(
				'nation' => $this->input->post('nation'),
				'address' => $this->input->post('address'),
				'telp' => $this->input->post('telp'),
				'fax' => $this->input->post('fax'),
				'email' => $this->input->post('email'),
				'map' => $this->input->post('map')
			);
		$this->page_m->create_contact($data);
		redirect('page/contact');
			
	}
	
	// contact update
	function contact_update($id_contact)
	{
		$data['contact_update']=$this->page_m->contact_update($id_contact);
		$this->load->view('backend/contact_update_v', $data);
	}
	
	// contact Update Process
	function contact_update_process($id_contact)
	{
		$data['contact_update_process']=$this->page_m->contact_update_process($id_contact);
		redirect('page/contact');
	}
	
	// contact Delete
	function contact_delete()
	{
		$this->page_m->contact_delete();
		redirect('page/contact');
	}
	
	
	// about
	function about()
	{
		$data['about']=$this->page_m->about();
		$data['about_banner']=$this->page_m->about_banner();
		$this->load->view('backend/about_v',$data);
	}
	
	// about
	function create_about()
	{
		$this->load->view('backend/about_create_v');
	}
	
	// Create about process 
	function create_about_process(){
		$this->load->library('image_lib');
		$url = './all_picture/';    //path image
		if($_FILES['gambar']['error']==3)  //if No file was uploaded.
		return false;  
			
		$config['upload_path'] = $url."about/";        
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']        = 2048; 
		
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		$files = $this->upload->data();
		
		if( $this->upload->do_upload('gambar',$config) )
		{    
			$files = $this->upload->data();

			$data = array('upload_data' => $this->upload->data());
			$hasil = str_replace (" ",  "_", $files['file_name']);
			
			$title_seo = seo($this->input->post('title'));
			$data=array(
					'title' => $this->input->post('title'),
					'title_seo' => $title_seo,
					'description' => $this->input->post('description'),
					'image_about' =>  $hasil
				);
			$this->page_m->create_about($data);
			redirect('page/about');
			
		}else{
			$this->load->view('backend/image_validasi_v');
		}
		
		
			
	}
	
	// about update
	function about_update($id_about)
	{
		$data['about_update']=$this->page_m->about_update($id_about);
		$this->load->view('backend/about_update_v', $data);
	}
	
	// about Update Process
	function about_update_process($id_about)
	{
		$data['about_update_process']=$this->page_m->about_update_process($id_about);
		redirect('page/about');
	}
	
	// about Delete
	function about_delete($id_about)
	{
		$this->page_m->about_delete($id_about);
		redirect('page/about');
	}
	
	
	
	// services
	function services()
	{
		$data['services']=$this->page_m->services();
		$this->load->view('backend/services_v',$data);
	}
	
	// services
	function create_services()
	{
		$this->load->view('backend/services_create_v');
	}
	
	// Create services process 
	function create_services_process(){
		$data=array(
				'title' => $this->input->post('title'),
				'description' => $this->input->post('description')
			);
		$this->page_m->create_services($data);
		redirect('page/services');
			
	}
	
	// services update
	function services_update($id_services)
	{
		$data['services_update']=$this->page_m->services_update($id_services);
		$this->load->view('backend/services_update_v', $data);
	}
	
	// services Update Process
	function services_update_process($id_services)
	{
		$data['services_update_process']=$this->page_m->services_update_process($id_services);
		redirect('page/services');
	}
	
	// services Delete
	function services_delete()
	{
		$this->page_m->services_delete();
		redirect('page/services');
	}


// journal
	function journal()
	{
		$data['journal']=$this->page_m->journal();
		$this->load->view('backend/journal_v',$data);
	}
	
	// Create journal
	function create_journal()
	{
		$data['projects_category']=$this->page_m->projects_category();
		$this->load->view('backend/journal_create_v', $data);
	}
	
	// Create journal
	function create_journal_process(){
		
		$this->load->library('image_lib');
		$url = './all_picture/journal/';    //path image
		if($_FILES['gambar']['error']==3)  //if No file was uploaded.
		return false;  
			
		$config['upload_path'] = $url."original/";        
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']        = 2048; 
		$lokasi_file    = $_FILES['gambar']['tmp_name'];
		
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		$files = $this->upload->data();
		$fileNameResize = $config['upload_path'].$files['file_name'];
		
		if( $this->upload->do_upload('gambar',$config) )
		{    
			$files = $this->upload->data();
			
			$fileNameResize = $config['upload_path'].$files['file_name'];
			$size =  array(                
						array('name'    => 'small','width'    => 100, 'height'    => 100, 'quality'    => '100%'),
						array('name'    => 'medium','width'    => 250, 'height'    => 250, 'quality'    => '100%')
					);
			$resize = array();
			foreach($size as $r){                
				$resize = array(
					"width"            => $r['width'],
					"height"        => $r['height'],
					"quality"        => $r['quality'],
					"source_image"    => $fileNameResize,
					"new_image"        => $url.$r['name'].'/'.$files['file_name']
				);
				$this->image_lib->initialize($resize);
				if(!$this->image_lib->resize())                    
					die($this->image_lib->display_errors());
			}

			$data = array('upload_data' => $this->upload->data());
			$hasil = str_replace (" ",  "_", $files['file_name']);
			$title_projects_seo = seo($this->input->post('title_projects'));
			$data=array(	
				'id_projects_category' => $this->input->post('projects_category'),
				'title_projects' => $this->input->post('title_projects'),
				'title_projects_seo' => $title_projects_seo,
				'image_projects' =>  $hasil,
				/*
				'description' => $this->input->post('description'),
				'status' => $this->input->post('status'),
				'location' => $this->input->post('location'),
				'team' => $this->input->post('team'),
				'photographer' => $this->input->post('photographer'),
				*/
				'all_image' => $this->input->post('all_image'),
				'journal' => $this->input->post('journal'),
				/*
				'awards' => $this->input->post('awards'),
				*/
				'meta_title' => $this->input->post('meta_title'),
				'meta_keywords' => $this->input->post('meta_keywords'),
				'meta_description' => $this->input->post('meta_description'),
'order_no' => $this->input->post('order_no'),
'date_projects' => $this->input->post('date_projects')
			);
			
			$this->page_m->create_journal($data);
			redirect('page/journal');
			
		}else{
			$this->load->view('backend/image_validasi_v');
		}

	}
	
	// journal update
	function journal_update($id_journal)
	{
		$data['journal_update']=$this->page_m->journal_update($id_journal);
		$data['projects_category']=$this->page_m->projects_category();
		$this->load->view('backend/journal_update_v', $data);
	}
	
	// journal Update Process
	function journal_update_process($id_journal)
	{
		$data['journal_update_process']=$this->page_m->journal_update_process($id_journal);
		redirect('page/journal');
	}

	// journal Delete
	function journal_delete($id_journal)
	{
		$this->page_m->journal_delete($id_journal);
		redirect('page/journal');
	}
	
	
	// logo
	function logo()
	{
		$data['logo']=$this->page_m->logo();
		$this->load->view('backend/logo_v',$data);
	}
	
	// logo update
	function logo_update($id_template)
	{
		$data['logo_update']=$this->page_m->logo_update($id_template);
		$this->load->view('backend/logo_update_v', $data);
	}
	
	// logo Update Process
	function logo_update_process($id_template)
	{
		$data['logo_update_process']=$this->page_m->logo_update_process($id_template);
		redirect('page/logo');
	}
	
	// about_banner update
	function about_banner_update($id_about_banner)
	{
		$data['about_banner_update']=$this->page_m->about_banner_update($id_about_banner);
		$this->load->view('backend/about_banner_update_v', $data);
	}
	
	// about_banner Update Process
	function about_banner_update_process($id_about_banner)
	{
		$data['about_banner_update_process']=$this->page_m->about_banner_update_process($id_about_banner);
		redirect('page/about_banner');
	}
	

	// designers
	function designers()
	{
		$data['designers']=$this->page_m->designers();
		$this->load->view('backend/designers_v',$data);
	}
	
	// designers
	function create_designers()
	{
		$this->load->view('backend/designers_create_v');
	}
	
	// Create designers process 
	function create_designers_process(){
		$this->load->library('image_lib');
		$url = './all_picture/';    //path image
		if($_FILES['gambar']['error']==3)  //if No file was uploaded.
		return false;  
			
		$config['upload_path'] = $url."designers/";        
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']        = 2048; 
		
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		$files = $this->upload->data();
		
		if( $this->upload->do_upload('gambar',$config) )
		{    
			$files = $this->upload->data();

			$data = array('upload_data' => $this->upload->data());
			$hasil = str_replace (" ",  "_", $files['file_name']);
			
			$data=array(
					'title' => $this->input->post('title'),
					'description' => $this->input->post('description'),
					'image_designers' =>  $hasil
				);
			$this->page_m->create_designers($data);
			redirect('page/designers');
			
		}else{
			$this->load->view('backend/image_validasi_v');
		}
		
		
			
	}
	
	// designers update
	function designers_update($id_designers)
	{
		$data['designers_update']=$this->page_m->designers_update($id_designers);
		$this->load->view('backend/designers_update_v', $data);
	}
	
	// designers Update Process
	function designers_update_process($id_designers)
	{
		$data['designers_update_process']=$this->page_m->designers_update_process($id_designers);
		redirect('page/designers');
	}
	
	// designers Delete
	function designers_delete($id_designers)
	{
		$this->page_m->designers_delete($id_designers);
		redirect('page/designers');
	}
	
	// blogs_category
	function blogs_category()
	{
		$data['blogs_category']=$this->page_m->blogs_category();
		$this->load->view('backend/blogs_category_v',$data);
	}
	
	// blogs_category
	function create_blogs_category()
	{
		$this->load->view('backend/blogs_category_create_v');
	}
	
	// Create blogs_category process 
	function create_blogs_category_process(){
		$title_blogs_category_seo = seo($this->input->post('title_blogs_category'));
		$data=array(
				'title_blogs_category' => $this->input->post('title_blogs_category'),
				'title_blogs_category_seo' => $title_blogs_category_seo,
				'publish' => $this->input->post('publish')
			);
		$this->page_m->create_blogs_category($data);
		redirect('page/blogs_category');
			
	}
	
	// blogs_category update
	function blogs_category_update($id_blogs_category)
	{
		$data['blogs_category_update']=$this->page_m->blogs_category_update($id_blogs_category);
		$this->load->view('backend/blogs_category_update_v', $data);
	}
	
	// blogs_category Update Process
	function blogs_category_update_process($id_blogs_category)
	{
		$data['blogs_category_update_process']=$this->page_m->blogs_category_update_process($id_blogs_category);
		redirect('page/blogs_category');
	}
	
	// blogs_category Delete
	function blogs_category_delete()
	{
		$this->page_m->blogs_category_delete();
		redirect('page/blogs_category');
	}
	
	
	// blogs
	function blogs()
	{
		$data['blogs']=$this->page_m->blogs();
		$this->load->view('backend/blogs_v',$data);
	}
	
	// Create blogs
	function create_blogs()
	{
		$data['blogs_category']=$this->page_m->blogs_category();
		$this->load->view('backend/blogs_create_v', $data);
	}
	
	// Create blogs
	function create_blogs_process(){
		
		$this->load->library('image_lib');
		$url = './all_picture/blogs/';    //path image
		if($_FILES['gambar']['error']==3)  //if No file was uploaded.
		return false;  
			
		$config['upload_path'] = $url."original/";        
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']        = 2048; 
		$lokasi_file    = $_FILES['gambar']['tmp_name'];
		
		$this->load->library('upload');
		$this->upload->initialize($config);
		
		$files = $this->upload->data();
		$fileNameResize = $config['upload_path'].$files['file_name'];
		
		if( $this->upload->do_upload('gambar',$config) )
		{    
			$files = $this->upload->data();
			$fileNameResize = $config['upload_path'].$files['file_name'];
				$size =  array(                
							array('name'    => 'small','width'    => 100, 'height'    => 100, 'quality'    => '100%'),
							array('name'    => 'medium','width'    => 500, 'height'    => 500, 'quality'    => '100%')
						);
				$resize = array();
				foreach($size as $r){                
					$resize = array(
						"width"            => $r['width'],
						"height"        => $r['height'],
						"quality"        => $r['quality'],
						"source_image"    => $fileNameResize,
						"new_image"        => $url.$r['name'].'/'.$files['file_name']
					);
					$this->image_lib->initialize($resize);
					if(!$this->image_lib->resize())                    
						die($this->image_lib->display_errors());
				}

			$data = array('upload_data' => $this->upload->data());
			$hasil = str_replace (" ",  "_", $files['file_name']);
			$title_blogs_seo = seo($this->input->post('title_blogs'));
			$data=array(	
				'id_blogs_category' => $this->input->post('blogs_category'),
				'title_blogs' => $this->input->post('title_blogs'),
				'title_blogs_seo' => $title_blogs_seo,
				'image_blogs' =>  $hasil,
				'description' => $this->input->post('description'),
				'meta_title' => $this->input->post('meta_title'),
				'meta_keywords' => $this->input->post('meta_keywords'),
				'meta_description' => $this->input->post('meta_description'),
				'created_by' => $this->session->userdata('name'),
			);
			
			$this->page_m->create_blogs($data);
			redirect('page/blogs');
			
		}else{
			$this->load->view('backend/image_validasi_v');
		}
	}
	
	// blogs update
	function blogs_update($id_blogs)
	{
		$data['blogs_update']=$this->page_m->blogs_update($id_blogs);
		$data['blogs_category']=$this->page_m->blogs_category();
		$this->load->view('backend/blogs_update_v', $data);
	}
	
	// blogs Update Process
	function blogs_update_process($id_blogs)
	{
		$data['blogs_update_process']=$this->page_m->blogs_update_process($id_blogs);
		redirect('page/blogs');
	}

	// blogs Delete
	function blogs_delete($id_blogs)
	{
		$this->page_m->blogs_delete($id_blogs);
		redirect('page/blogs');
	}




	// careers
	function careers()
	{
		$data['careers']=$this->page_m->careers();
		$this->load->view('backend/careers_v',$data);
	}
	
	// careers
	function create_careers()
	{
		$this->load->view('backend/careers_create_v');
	}
	
	// Create careers process 
	function create_careers_process(){
		$data=array(
				'position' => $this->input->post('position'),
				'description' => $this->input->post('description')
			);
		$this->page_m->create_careers($data);
		redirect('page/careers');
			
	}
	
	// careers update
	function careers_update($id_careers)
	{
		$data['careers_update']=$this->page_m->careers_update($id_careers);
		$this->load->view('backend/careers_update_v', $data);
	}
	
	// careers Update Process
	function careers_update_process($id_careers)
	{
		$data['careers_update_process']=$this->page_m->careers_update_process($id_careers);
		redirect('page/careers');
	}
	
	// careers Delete
	function careers_delete()
	{
		$this->page_m->careers_delete();
		redirect('page/careers');
	}
	
}
