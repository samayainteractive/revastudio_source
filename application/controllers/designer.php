<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Designer extends CI_Controller {

	// Construct
	public function __construct()
	{
		parent::__construct();
		$this->load->model('access_m');
		
	}

	public function index()
	{
		$data['designer'] = $this->access_m->getAllDesigner();
		$data['newsCategory'] = $this->access_m->getNewsCategory();
		
		$this->load->view('designer_v', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */