<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	// Construct
	public function __construct()
	{
		parent::__construct();
		$this->load->model('access_m');
		
	}

	public function index($id)
	{
		$data['news'] = $this->access_m->getDataNewsCategory($id);
		$data['newsCategory'] = $this->access_m->getNewsCategory();

		$this->load->view('news_v', $data);
	}

	function details($id){

		$data['newsSelected'] = $this->access_m->getNewsSelected($id);
		$data['newsCategory'] = $this->access_m->getNewsCategory();

		$this->load->view('news_details_v', $data);	
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */