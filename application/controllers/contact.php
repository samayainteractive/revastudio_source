<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

	// Construct
	public function __construct()
	{
		parent::__construct();
		$this->load->model('access_m');
		
	}

	public function index()
	{
		$data['contact'] = $this->access_m->getContact();
		$data['newsCategory'] = $this->access_m->getNewsCategory();

		$this->load->view('contact_v', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */